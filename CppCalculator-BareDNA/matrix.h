#pragma once

#ifndef MATRIX_H_
#define MATRIX_H_

#include <vector>
#include <cmath>
#include <bitset>
#include <string>
#include <Eigen/Core>

#include "input.h"

using std::vector;
using std::string;
using Eigen::MatrixXd;

using vector2d = vector< vector<double> >;

MatrixXd create_first_boundary_matrix_v1(int max_mode);
MatrixXd create_matrix_V(int max_mode);
vector<MatrixXd> create_matrix_block_SB_SL_SP_SS_torque_v2(vector2d &L_1_0, valarray<double> &coeff_r_total_B,
	vector2d &L_1_a_B, vector2d &L_1_b_B_f, valarray<double> &coeff_r_total_L, vector2d &L_1_a_L, vector2d &L_1_b_L_f,
	valarray<double> &coeff_r_total_P, vector2d &L_1_a_P, vector2d &L_1_b_P_f, valarray<double> &coeff_r_total_S,
	vector2d &L_1_a_S, vector2d &L_1_b_P_S, vector<double> &w3j_all, int max_mode);
MatrixXd create_transfer_matrix_torque_v2(Params &p, double mu_L, double mu_P, double mu_S, int max_mode, const vector<MatrixXd> &S_BLPS);
MatrixXd quick_matrices_product_v3(MatrixXd &S, int N, double &log_coeff);
MatrixXd create_second_boundary_matrix_torque_v3(Params &p, double mu_L, double mu_P, double mu_S, double f, int max_mode, MatrixXd &V);

#endif