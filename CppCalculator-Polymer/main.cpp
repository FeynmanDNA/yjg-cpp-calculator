/*
 * A C++ implementation of the Matlab script for calculating DNA response
 * to force and torque constraints by Artem Yefremov
 * 
 * Implemented by Ladislav Hovan
 */

#include "main.h"

// Params intentionally passed by value, although it might be more efficient to split the struct
void calculate(int count, vector2d const &ft_values, vector2d &vvdResults, int core, Input const &input, Params p,
	vector<double> &w3j_all, vector2d &L_1_0, vector2d &L_1_a_B, vector2d &L_1_a_L, vector2d &L_1_a_P, vector2d &L_1_a_S, 
	MatrixXd &U, MatrixXd &V)
{
	vector<double> vdTemp(9);

	if (count > 0)
		recalculateForceTorque(p, ft_values[count][0], ft_values[count][1], input.max_mode);

	vdTemp[0] = ft_values[count][0];
	vdTemp[1] = ft_values[count][1];

	vector2d L_1_b_B_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_B * p.f, p.cutoff);
	vector2d L_1_b_B_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_B * p.f_plus, p.cutoff);
	vector2d L_1_b_B_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_B * p.f_minus, p.cutoff);
	// std::cout << "All L_k_r_b_B_f functions found\n";

	vector2d L_1_b_L_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_L * p.f, p.cutoff);
	vector2d L_1_b_L_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_L * p.f_plus, p.cutoff);
	vector2d L_1_b_L_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_L * p.f_minus, p.cutoff);
	// std::cout << "All L_k_r_b_L_f functions found\n";

	vector2d L_1_b_P_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_P * p.f, p.cutoff);
	vector2d L_1_b_P_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_P * p.f_plus, p.cutoff);
	vector2d L_1_b_P_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_P * p.f_minus, p.cutoff);
	// std::cout << "All L_k_r_b_P_f functions found\n";

	vector2d L_1_b_S_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_S * p.f, p.cutoff);
	vector2d L_1_b_S_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_S * p.f_plus, p.cutoff);
	vector2d L_1_b_S_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_S * p.f_minus, p.cutoff);
	// std::cout << "All L_k_r_b_S_f functions found\n\n";

	vector<MatrixXd> S_BLPS_plus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B, L_1_a_B,
		L_1_b_B_f_plus, p.coeff_r_total_L, L_1_a_L, L_1_b_L_f_plus, p.coeff_r_total_P, L_1_a_P, L_1_b_P_f_plus,
		p.coeff_r_total_S, L_1_a_S, L_1_b_S_f_plus, w3j_all, input.max_mode);
	vector<MatrixXd> S_BLPS_minus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B, L_1_a_B,
		L_1_b_B_f_minus, p.coeff_r_total_L, L_1_a_L, L_1_b_L_f_minus, p.coeff_r_total_P, L_1_a_P, L_1_b_P_f_minus,
		p.coeff_r_total_S, L_1_a_S, L_1_b_S_f_minus, w3j_all, input.max_mode);

	MatrixXd S_plus = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode, S_BLPS_plus);
	MatrixXd S_minus = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode, S_BLPS_minus);

	double coeff_plus_prod, coeff_minus_prod;
	MatrixXd S_plus_prod = quick_matrices_product_v3(S_plus, p.N - 1, coeff_plus_prod);
	MatrixXd S_minus_prod = quick_matrices_product_v3(S_minus, p.N - 1, coeff_minus_prod);

	MatrixXd O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f_plus, input.max_mode, V);
	MatrixXd O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f_minus, input.max_mode, V);

	complex<double> trace_plus = (U * S_plus_prod * O_plus).trace();
	complex<double> trace_minus = (U * S_minus_prod * O_minus).trace();

	complex<double> ext = (coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus)) /
		(p.f_plus - p.f_minus) / input.DNA_length;
	double DNA_rel_extension = ext.real();
	double DNA_extension = DNA_rel_extension * input.DNA_length;

	vdTemp[2] = DNA_extension;
	vdTemp[3] = DNA_rel_extension;

	vector<MatrixXd> S_BLPS = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B, L_1_a_B, L_1_b_B_f,
		p.coeff_r_total_L, L_1_a_L, L_1_b_L_f, p.coeff_r_total_P, L_1_a_P, L_1_b_P_f, p.coeff_r_total_S, L_1_a_S,
		L_1_b_S_f, w3j_all, input.max_mode);

	S_plus = create_transfer_matrix_torque_v2(p, p.mu_L_plus, p.mu_P, p.mu_S, input.max_mode, S_BLPS);
	S_minus = create_transfer_matrix_torque_v2(p, p.mu_L_minus, p.mu_P, p.mu_S, input.max_mode, S_BLPS);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.N - 1, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.N - 1, coeff_minus_prod);

	O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L_plus, p.mu_P, p.mu_S, p.f, input.max_mode, V);
	O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L_minus, p.mu_P, p.mu_S, p.f, input.max_mode, V);

	trace_plus = (U * S_plus_prod * O_plus).trace();
	trace_minus = (U * S_minus_prod * O_minus).trace();

	complex<double> occ = -(coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus))
		/ ((p.mu_L_plus - p.mu_L_minus) * p.q * p.N);
	double DNA_L_occupancy = std::abs(occ.real());

	vdTemp[4] = DNA_L_occupancy;

	S_plus = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P_plus, p.mu_S, input.max_mode, S_BLPS);
	S_minus = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P_minus, p.mu_S, input.max_mode, S_BLPS);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.N - 1, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.N - 1, coeff_minus_prod);

	O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P_plus, p.mu_S, p.f, input.max_mode, V);
	O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P_minus, p.mu_S, p.f, input.max_mode, V);

	trace_plus = (U * S_plus_prod * O_plus).trace();
	trace_minus = (U * S_minus_prod * O_minus).trace();

	occ = -(coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus)) /
		((p.mu_P_plus - p.mu_P_minus) * p.q * p.N);
	double DNA_P_occupancy = std::abs(occ.real());

	vdTemp[5] = DNA_P_occupancy;

	S_plus = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S_plus, input.max_mode, S_BLPS);
	S_minus = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S_minus, input.max_mode, S_BLPS);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.N - 1, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.N - 1, coeff_minus_prod);

	O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S_plus, p.f, input.max_mode, V);
	O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S_minus, p.f, input.max_mode, V);

	trace_plus = (U * S_plus_prod * O_plus).trace();
	trace_minus = (U * S_minus_prod * O_minus).trace();

	occ = -(coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus)) /
		((p.mu_S_plus - p.mu_S_minus) * p.q * p.N);
	double DNA_S_occupancy = std::abs(occ.real());

	vdTemp[6] = DNA_S_occupancy;

	S_BLPS_plus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B_plus, L_1_a_B, L_1_b_B_f,
		p.coeff_r_total_L_plus, L_1_a_L, L_1_b_L_f, p.coeff_r_total_P_plus, L_1_a_P, L_1_b_P_f, p.coeff_r_total_S_plus,
		L_1_a_S, L_1_b_S_f, w3j_all, input.max_mode);
	S_BLPS_minus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B_minus, L_1_a_B, L_1_b_B_f,
		p.coeff_r_total_L_minus, L_1_a_L, L_1_b_L_f, p.coeff_r_total_P_minus, L_1_a_P, L_1_b_P_f, p.coeff_r_total_S_minus,
		L_1_a_S, L_1_b_S_f, w3j_all, input.max_mode);

	MatrixXd S_plus_a = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS_plus[0], S_BLPS[1], S_BLPS[2], S_BLPS[3] });
	MatrixXd S_plus_b = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS[0], S_BLPS_plus[1], S_BLPS[2], S_BLPS[3] });
	MatrixXd S_plus_c = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS_plus[2], S_BLPS[3] });
	MatrixXd S_plus_d = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS_plus[3] });

	MatrixXd S_minus_a = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS_minus[0], S_BLPS[1], S_BLPS[2], S_BLPS[3] });
	MatrixXd S_minus_b = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS[0], S_BLPS_minus[1], S_BLPS[2], S_BLPS[3] });
	MatrixXd S_minus_c = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS_minus[2], S_BLPS[3] });
	MatrixXd S_minus_d = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS_minus[3] });

	double coeff_plus_prod_a, coeff_plus_prod_b, coeff_plus_prod_c, coeff_plus_prod_d;
	MatrixXd S_plus_prod_a = quick_matrices_product_v3(S_plus_a, p.N - 1, coeff_plus_prod_a);
	MatrixXd S_plus_prod_b = quick_matrices_product_v3(S_plus_b, p.N - 1, coeff_plus_prod_b);
	MatrixXd S_plus_prod_c = quick_matrices_product_v3(S_plus_c, p.N - 1, coeff_plus_prod_c);
	MatrixXd S_plus_prod_d = quick_matrices_product_v3(S_plus_d, p.N - 1, coeff_plus_prod_d);

	double coeff_minus_prod_a, coeff_minus_prod_b, coeff_minus_prod_c, coeff_minus_prod_d;
	MatrixXd S_minus_prod_a = quick_matrices_product_v3(S_minus_a, p.N - 1, coeff_minus_prod_a);
	MatrixXd S_minus_prod_b = quick_matrices_product_v3(S_minus_b, p.N - 1, coeff_minus_prod_b);
	MatrixXd S_minus_prod_c = quick_matrices_product_v3(S_minus_c, p.N - 1, coeff_minus_prod_c);
	MatrixXd S_minus_prod_d = quick_matrices_product_v3(S_minus_d, p.N - 1, coeff_minus_prod_d);

	MatrixXd O = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f, input.max_mode, V);

	complex<double> trace_plus_a = (U * S_plus_prod_a * O).trace();
	complex<double> trace_plus_b = (U * S_plus_prod_b * O).trace();
	complex<double> trace_plus_c = (U * S_plus_prod_c * O).trace();
	complex<double> trace_plus_d = (U * S_plus_prod_d * O).trace();

	complex<double> trace_minus_a = (U * S_minus_prod_a * O).trace();
	complex<double> trace_minus_b = (U * S_minus_prod_b * O).trace();
	complex<double> trace_minus_c = (U * S_minus_prod_c * O).trace();
	complex<double> trace_minus_d = (U * S_minus_prod_d * O).trace();

	complex<double> linking_number_a = (coeff_plus_prod_a - coeff_minus_prod_a + std::log(trace_plus_a) -
		std::log(trace_minus_a)) / ((p.B_coeff_plus - p.B_coeff_minus) * 2 * pi);
	complex<double> linking_number_b = (coeff_plus_prod_b - coeff_minus_prod_b + std::log(trace_plus_b) -
		std::log(trace_minus_b)) / ((p.L_coeff_plus - p.L_coeff_minus) * 2 * pi);
	complex<double> linking_number_c = (coeff_plus_prod_c - coeff_minus_prod_c + std::log(trace_plus_c) -
		std::log(trace_minus_c)) / ((p.P_coeff_plus - p.P_coeff_minus) * 2 * pi);
	complex<double> linking_number_d = (coeff_plus_prod_d - coeff_minus_prod_d + std::log(trace_plus_d) -
		std::log(trace_minus_d)) / ((p.S_coeff_plus - p.S_coeff_minus) * 2 * pi);
	complex<double> linking_number = linking_number_a + linking_number_b + linking_number_c + linking_number_d + p.lk_L_0 * p.q * p.N * DNA_L_occupancy + p.lk_P_0 * p.q * p.N * DNA_P_occupancy + p.lk_S_0 * p.q * p.N * DNA_S_occupancy;

	double DNA_linking = real(linking_number);
	double DNA_superhelical_density = DNA_linking / (input.DNA_length / 3.4);

	vdTemp[7] = DNA_linking;
	vdTemp[8] = DNA_superhelical_density;

	std::swap(vvdResults[core], vdTemp);
}

int main(int argc, char *argv[])
{
	Timer global;

	vector2d ft_values;
	Input input;

	// Input
	if (argc == 5)
	{
		ft_values = loadForceTorqueFile(argv[2]);
		input.max_mode = std::atoi(argv[3]);
		input.n_threads = std::atoi(argv[4]);
	}
	else if (argc == 6)
	{
		ft_values.push_back({ std::atof(argv[2]), std::atof(argv[3]) });
		input.max_mode = std::atoi(argv[4]);
		input.n_threads = std::atoi(argv[5]);
	}
	else
	{
		std::cerr << "The program requires one of two input styles:\n";
		std::cerr << "1. DNA length, force, torque, number of modes and number of threads (5 inputs)\n";
		std::cerr << "2. DNA length, name of file containing force and torque values, number of modes" <<
			" and number of threads (4 inputs)\n";
		exit(2);
	}

	input.DNA_length = std::atof(argv[1]);

	// Calculating parametres from given input
	input.force = ft_values[0][0];
	input.torque = ft_values[0][1];
	Params p = initialiseParams(input);

	vector<double> w3j_all = all_Wigner_3j_symbols_torque(input.max_mode - 1);
	// std::cout << "All w3j symbols found\n";

	vector2d L_1_0 = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, 0, p.cutoff);
	vector2d L_1_a_B = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_B, p.cutoff);
	// std::cout << "All L_k_r_B and L_k_r_a_B functions found\n";
	
	vector2d L_1_a_L = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_L, p.cutoff);
	// std::cout << "All L_k_r_a_L functions found\n";

	vector2d L_1_a_P = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_P, p.cutoff);
	// std::cout << "All L_k_r_a_P functions found\n";

	vector2d L_1_a_S = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_S, p.cutoff);
	// std::cout << "All L_k_r_a_S functions found\n";

	MatrixXd U = create_first_boundary_matrix_v1(input.max_mode);
	MatrixXd V = create_matrix_V(input.max_mode);

	// Output file
	std::ofstream Output;
	if (argc == 5)
	{
		//backupFile("output.dat");
		Output.open("output.csv");
		Output << "# NUS Yan Jie Group @ 2018\n";
		Output << "# force, torque, extension, rel_extension, L_occupancy, P_occupancy, S_occupancy, linking, superhelical\n";
	}

	int count = 0;
	bool has_negative = false;
	while (count < ft_values.size())
	{
		vector2d vvdMetaResults;
		vector<thread> vtThreads;
		for (int core = 0; core < input.n_threads; ++core)
		{
			vvdMetaResults.emplace_back();
			thread temp(calculate, count, std::ref(ft_values), std::ref(vvdMetaResults), core, std::ref(input), 
				p, std::ref(w3j_all), std::ref(L_1_0), std::ref(L_1_a_B), std::ref(L_1_a_L), std::ref(L_1_a_P), 
				std::ref(L_1_a_S), std::ref(U), std::ref(V));
			vtThreads.push_back(thread(std::move(temp)));
			++count;
			if (count == ft_values.size())
				break;
		}
		for (auto &thread : vtThreads)
			thread.join();
		for (auto &values : vvdMetaResults)
		{
			if (Output)
			{
				for (int nPos = 0; nPos < values.size(); ++nPos)
				{
					Output << values[nPos];
					// Extension
					if (nPos == 2 && values[nPos] < 0.0)
						has_negative = true;
					if (nPos < values.size() - 1)
						Output << ",";
				}
				Output << "\n";
			}
		}
	}

	if (Output)
	{
		if (has_negative)
			Output << "# The file contains negative extension values\n" <<
			"# These result from the DNA being in supercoiled state, which our formula doesn't cover\n" <<
			"# Effective extension for these cases can be considered zero\n";
		Output.close();
	}
	double runtime = global.elapsed();
	std::cout << "Total t: " << runtime << "s\n";
	if (ft_values.size() > 1)
		std::cout << "Average t / calculation: " << runtime / ft_values.size() << "s\n";

	return 0;
}