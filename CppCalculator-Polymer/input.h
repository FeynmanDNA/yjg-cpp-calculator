#pragma once

#ifndef INPUT_H_
#define INPUT_H_

#include <cmath>
#include <valarray>
#include <fstream>
#include <string>
#include <cctype>
#include <vector>
#include <sstream>
#include <algorithm>
//#include <experimental/filesystem>

//#include <boost/filesystem.hpp>
#include <boost/math/constants/constants.hpp>
#include "functions.h"

using std::valarray;
using std::string;
using std::vector;

using vector2d = vector< vector<double> >;
//namespace fs = std::experimental::filesystem;
//namespace fs = boost::filesystem;

extern double pi;

struct Input
{
	double force = 0.1;        // in[pN] units
	double torque = 0.0;       // in[pN*nm] units
	double DNA_length = 3400;  // DNA length in B - DNA state in[nm] units
	int max_mode = 14;         // number of modes taken into account in the transfer matrix calculations (~10 - 15)
	int n_threads = 1;         // number of concurrent threads to be launched
};

struct Params
{
	// *** Default values ***
	// B-DNA (Polymer for Polymer version)
	double b_B = 0.1;          // size of individual DNA segment, b, in B - DNA state in[nm] units
	double A_B = 10.0;         // B - DNA bending persistence length
	double C_B = 0.1;         // B - DNA twisting persistence length 							   
	double lambda_B = 4.3;     // lambda parameter for B - DNA
	// L-DNA
	double b_L = 1.35 * b_B;      // size of individual DNA segment in L - DNA state
	double A_L = 0.1;             // L - DNA bending persistence length   			  
	double C_L = 0.1;            // L - DNA twisting persistence length					  
	double lambda_L = 4.3;        // lambda parameter for L - DNA
	double mu_L = 100.0;            // the energy difference in k_B*T units at zero torque per single DNA base - pair corresponding to the transition from B - DNA to L - DNA
	double lk_L_0 = -1.0 / 16 - 1 / 10.4;  // the DNA relaxed linking number change per base - pair during transition from B - DNA to L - DNA, 16 bp - helical repeat of L - DNA, 10.5 bp - helical repeat of B - DNA
	double coop = 9.0;            // energy penalty appearing due to B - L - DNA, B - P - DNA and P - L - DNA boundaries
	// P-DNA
	double b_P = 1.7 * b_B;           // size of individual DNA segment in P - DNA state
	double A_P = 0.1;                // P - DNA bending persistence length
	double C_P = 0.1;                // P - DNA twisting persistence length       
	double lambda_P = -0.5;           // lambda parameter for P - DNA
	double mu_P = 100.0;               // the energy difference in k_B*T units at zero torque per single DNA base - pair corresponding to the transition from B - DNA to P - DNA
	double lk_P_0 = 1.0 / 3 - 1 / 10.4;     // the DNA relaxed linking number change per base - pair during transition from B - DNA to P - DNA, 3 bp - helical repeat of P - DNA, 10.5 bp - helical repeat of B - DNA
	// S-DNA
	double b_S = 1.7 * b_B;           // size of individual DNA segment in S - DNA state
	double A_S = 0.1;          // S - DNA bending persistence length
	double C_S = 0.1;          // S - DNA twisting persistence length
	double lambda_S = 4.3;            // lambda parameter for S - DNA
	double mu_S = 100.0;                // the energy difference in k_B*T units at zero torque per single DNA base - pair corresponding to the transition from B - DNA to S - DNA
	double lk_S_0 = 1.0 / 35.0 - 1.0 / 10.4;     // the DNA relaxed linking number change per base - pair during transition from B - DNA to S - DNA, 35 bp - helical repeat of S - DNA, 10.4 bp - helical repeat of B - DNA

	// Other
	double cutoff = pi;
	double plus_coeff = 1.0001;
	double minus_coeff = 0.9999;

	// *** Calculated after default adjustments ***
	double a_B;  // B - DNA bending elasticity
	double c_B;  // B - DNA twisting elasticity
	double q;    // number of DNA base - pairs in individual DNA segment
	double a_L;  // L - DNA bending elasticity
	double c_L;  // L - DNA twisting elasticity
	double a_P;  // P - DNA bending elasticity
	double c_P;  // P - DNA twisting elasticity
	double a_S;  // S - DNA bending elasticity
	double c_S;  // S - DNA twisting elasticity
	double mu_L_plus;
	double mu_L_minus;
	double mu_P_plus;
	double mu_P_minus;
	double mu_S_plus;
	double mu_S_minus;

	// *** Derived from input ***
	int N;  // total number of DNA segments in the polymer chain
	double tau;                     // torque / k_B / T
	double f;                       // force / k_B / T
	double chi_B;  // chi variable in the transfer matrix formula
	double chi_L;
	double chi_P;
	double chi_S;
	double omega_B;    // omega variable in the transfer matrix formula
	double omega_L;
	double omega_P;
	double omega_S;
	double B_coeff;
	double L_coeff;
	double P_coeff;
	double S_coeff;
	double f_plus;
	double f_minus;
	double B_coeff_plus;
	double B_coeff_minus;
	double L_coeff_plus;
	double L_coeff_minus;
	double P_coeff_plus;
	double P_coeff_minus;
	double S_coeff_plus;
	double S_coeff_minus;
	valarray<double> besseli_coeff_B;
	valarray<double> besseli_coeff_plus_B;
	valarray<double> besseli_coeff_minus_B;
	valarray<double> besseli_coeff_L;
	valarray<double> besseli_coeff_plus_L;
	valarray<double> besseli_coeff_minus_L;
	valarray<double> besseli_coeff_P;
	valarray<double> besseli_coeff_plus_P;
	valarray<double> besseli_coeff_minus_P;
	valarray<double> besseli_coeff_S;
	valarray<double> besseli_coeff_plus_S;
	valarray<double> besseli_coeff_minus_S;
	valarray<double> besseli_c_B_torque;
	valarray<double> besseli_c_L_torque;
	valarray<double> besseli_c_P_torque;
	valarray<double> besseli_c_S_torque;
	valarray<double> coeff_omega_B_all;
	valarray<double> coeff_omega_L_all;
	valarray<double> coeff_omega_P_all;
	valarray<double> coeff_omega_S_all;
	valarray<double> coeff_r_total_B;
	valarray<double> coeff_r_total_B_plus;
	valarray<double> coeff_r_total_B_minus;
	valarray<double> coeff_r_total_L;
	valarray<double> coeff_r_total_L_plus;
	valarray<double> coeff_r_total_L_minus;
	valarray<double> coeff_r_total_P;
	valarray<double> coeff_r_total_P_plus;
	valarray<double> coeff_r_total_P_minus;
	valarray<double> coeff_r_total_S;
	valarray<double> coeff_r_total_S_plus;
	valarray<double> coeff_r_total_S_minus;
};

bool isEmpty(string strInput);
string trimWhitespace(string strInput);
void assignInput(string strLine, Params &p);
void recalculateForceTorque(Params &p, double force, double torque, int max_mode);
Params initialiseParams(Input &input);
vector2d loadForceTorqueFile(string filename);
//void backupFile(string strFilename);

#endif