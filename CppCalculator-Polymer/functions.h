#pragma once

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <cmath>
#include <valarray>
#include <vector>
#include <complex>

#include <boost/math/special_functions/bessel.hpp>
#include <boost/math/quadrature/gauss_kronrod.hpp>
#include "input.h"
#include "wigner.h"

using std::valarray;
using std::vector;
using std::complex;

using namespace std::complex_literals;

using vector2d = vector< vector<double> >;

valarray<double> find_all_Besseli_r(double x, int max_n);
int index(int i1, int i2, int i3, int i4, int max_n); 
vector<double> all_Wigner_3j_symbols_torque(int max_n);
long double factorial(int n);
double j_polynomial_my(double n, double alpha, double beta, double x);
double P_n_ml_v2(double n, double m, double l, double theta);
double K_function(double x, double n, double l, double a);
double L_k_r_v3(double n, double l, double a, double cutoff);
vector2d find_indexed_L_k_r_v2(int max_n, int max_l, double a, double cutoff);

#endif
