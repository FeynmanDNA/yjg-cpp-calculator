#include "input.h"

double pi = boost::math::constants::pi<double>();
 
void recalculateForceTorque(Params &p, double force, double torque, int max_mode)
{
	p.tau = torque / 4.1;
	p.f = force / 4.1;
	p.f_plus = p.plus_coeff * p.f;
	p.f_minus = p.minus_coeff * p.f;

	p.chi_B = p.tau * p.lambda_B / 2 / pi / p.c_B;
	p.chi_L = p.tau * p.lambda_L / 2 / pi / p.c_L;
	p.chi_P = p.tau * p.lambda_P / 2 / pi / p.c_P;
	p.chi_S = p.tau * p.lambda_S / 2 / pi / p.c_S;
	p.chi_B_insert = p.tau * p.lambda_B_insert / 2 / pi / p.c_B_insert;
	p.omega_B = atan(p.chi_B);
	p.omega_L = atan(p.chi_L);
	p.omega_P = atan(p.chi_P);
	p.omega_S = atan(p.chi_S);
	p.omega_B_insert = atan(p.chi_B_insert);
	p.B_coeff = (1 - p.lambda_B / 2 / pi) * p.tau;
	p.L_coeff = (1 - p.lambda_L / 2 / pi) * p.tau;
	p.P_coeff = (1 - p.lambda_P / 2 / pi) * p.tau;
	p.S_coeff = (1 - p.lambda_S / 2 / pi) * p.tau;
	p.B_coeff_insert = (1 - p.lambda_B_insert / 2 / pi) * p.tau;

	if (std::abs(torque) > 0 && std::abs(p.B_coeff) > 0)
	{
		p.B_coeff_plus = p.plus_coeff * p.B_coeff;
		p.B_coeff_minus = p.minus_coeff * p.B_coeff;
	}
	else
	{
		p.B_coeff_plus = p.plus_coeff - 1;
		p.B_coeff_minus = p.minus_coeff - 1;
	}
	if (std::abs(torque) > 0 && std::abs(p.L_coeff) > 0)
	{
		p.L_coeff_plus = p.plus_coeff * p.L_coeff;
		p.L_coeff_minus = p.minus_coeff * p.L_coeff;
	}
	else
	{
		p.L_coeff_plus = p.plus_coeff - 1;
		p.L_coeff_minus = p.minus_coeff - 1;
	}
	if (std::abs(torque) > 0 && std::abs(p.P_coeff) > 0)
	{
		p.P_coeff_plus = p.plus_coeff * p.P_coeff;
		p.P_coeff_minus = p.minus_coeff * p.P_coeff;
	}
	else
	{
		p.P_coeff_plus = p.plus_coeff - 1;
		p.P_coeff_minus = p.minus_coeff - 1;
	}
	if (std::abs(torque) > 0 && std::abs(p.S_coeff) > 0)
	{
		p.S_coeff_plus = p.plus_coeff * p.S_coeff;
		p.S_coeff_minus = p.minus_coeff * p.S_coeff;
	}
	else
	{
		p.S_coeff_plus = p.plus_coeff - 1;
		p.S_coeff_minus = p.minus_coeff - 1;
	}
	if (std::abs(torque) > 0 && std::abs(p.B_coeff_insert) > 0)
	{
		p.B_coeff_plus_insert = p.plus_coeff * p.B_coeff_insert;
		p.B_coeff_minus_insert = p.minus_coeff * p.B_coeff_insert;
	}
	else
	{
		p.B_coeff_plus_insert = p.plus_coeff - 1;
		p.B_coeff_minus_insert = p.minus_coeff - 1;
	}

	p.besseli_coeff_B = find_all_Besseli_r(p.B_coeff, max_mode - 1);
	p.besseli_coeff_plus_B = find_all_Besseli_r(p.B_coeff_plus, max_mode - 1);
	p.besseli_coeff_minus_B = find_all_Besseli_r(p.B_coeff_minus, max_mode - 1);
	p.besseli_coeff_L = find_all_Besseli_r(p.L_coeff, max_mode - 1);
	p.besseli_coeff_plus_L = find_all_Besseli_r(p.L_coeff_plus, max_mode - 1);
	p.besseli_coeff_minus_L = find_all_Besseli_r(p.L_coeff_minus, max_mode - 1);
	p.besseli_coeff_P = find_all_Besseli_r(p.P_coeff, max_mode - 1);
	p.besseli_coeff_plus_P = find_all_Besseli_r(p.P_coeff_plus, max_mode - 1);
	p.besseli_coeff_minus_P = find_all_Besseli_r(p.P_coeff_minus, max_mode - 1);
	p.besseli_coeff_S = find_all_Besseli_r(p.S_coeff, max_mode - 1);
	p.besseli_coeff_plus_S = find_all_Besseli_r(p.S_coeff_plus, max_mode - 1);
	p.besseli_coeff_minus_S = find_all_Besseli_r(p.S_coeff_minus, max_mode - 1);
	p.besseli_coeff_B_insert = find_all_Besseli_r(p.B_coeff_insert, max_mode - 1);
	p.besseli_coeff_plus_B_insert = find_all_Besseli_r(p.B_coeff_plus_insert, max_mode - 1);
	p.besseli_coeff_minus_B_insert = find_all_Besseli_r(p.B_coeff_minus_insert, max_mode - 1);
	p.besseli_c_B_torque = find_all_Besseli_r(p.c_B * std::sqrt(1 + std::pow(p.chi_B, 2)), max_mode - 1);
	p.besseli_c_L_torque = find_all_Besseli_r(p.c_L * std::sqrt(1 + std::pow(p.chi_L, 2)), max_mode - 1);
	p.besseli_c_P_torque = find_all_Besseli_r(p.c_P * std::sqrt(1 + std::pow(p.chi_P, 2)), max_mode - 1);
	p.besseli_c_S_torque = find_all_Besseli_r(p.c_S * std::sqrt(1 + std::pow(p.chi_S, 2)), max_mode - 1);
	p.besseli_c_B_torque_insert = find_all_Besseli_r(p.c_B_insert * std::sqrt(1 + std::pow(p.chi_B_insert, 2)),
		max_mode - 1);

	p.coeff_omega_B_all = valarray<double>(1.0, max_mode);
	p.coeff_omega_L_all = valarray<double>(1.0, max_mode);
	p.coeff_omega_P_all = valarray<double>(1.0, max_mode);
	p.coeff_omega_S_all = valarray<double>(1.0, max_mode);
	p.coeff_omega_B_all_insert = valarray<double>(1.0, max_mode);

	for (int i = 1; i <= max_mode - 1; ++i)
	{
		p.coeff_omega_B_all[i] = 2 * cos(i*(p.omega_B - pi / 2));
		p.coeff_omega_L_all[i] = 2 * cos(i*(p.omega_L - pi / 2));
		p.coeff_omega_P_all[i] = 2 * cos(i*(p.omega_P - pi / 2));
		p.coeff_omega_S_all[i] = 2 * cos(i*(p.omega_S - pi / 2));
		p.coeff_omega_B_all_insert[i] = 2 * cos(i*(p.omega_B_insert - pi / 2));
	}

	p.coeff_r_total_B = p.coeff_omega_B_all * p.besseli_coeff_B * p.besseli_c_B_torque;
	p.coeff_r_total_B_plus = p.coeff_omega_B_all * p.besseli_coeff_plus_B * p.besseli_c_B_torque;
	p.coeff_r_total_B_minus = p.coeff_omega_B_all * p.besseli_coeff_minus_B * p.besseli_c_B_torque;
	p.coeff_r_total_L = p.coeff_omega_L_all * p.besseli_coeff_L * p.besseli_c_L_torque;
	p.coeff_r_total_L_plus = p.coeff_omega_L_all * p.besseli_coeff_plus_L * p.besseli_c_L_torque;
	p.coeff_r_total_L_minus = p.coeff_omega_L_all * p.besseli_coeff_minus_L * p.besseli_c_L_torque;
	p.coeff_r_total_P = p.coeff_omega_P_all * p.besseli_coeff_P * p.besseli_c_P_torque;
	p.coeff_r_total_P_plus = p.coeff_omega_P_all * p.besseli_coeff_plus_P * p.besseli_c_P_torque;
	p.coeff_r_total_P_minus = p.coeff_omega_P_all * p.besseli_coeff_minus_P * p.besseli_c_P_torque;
	p.coeff_r_total_S = p.coeff_omega_S_all * p.besseli_coeff_S * p.besseli_c_S_torque;
	p.coeff_r_total_S_plus = p.coeff_omega_S_all * p.besseli_coeff_plus_S * p.besseli_c_S_torque;
	p.coeff_r_total_S_minus = p.coeff_omega_S_all * p.besseli_coeff_minus_S * p.besseli_c_S_torque;
	p.coeff_r_total_B_insert = p.coeff_omega_B_all_insert * p.besseli_coeff_B_insert * p.besseli_c_B_torque_insert;
	p.coeff_r_total_B_plus_insert = p.coeff_omega_B_all_insert * p.besseli_coeff_plus_B_insert *
		p.besseli_c_B_torque_insert;
	p.coeff_r_total_B_minus_insert = p.coeff_omega_B_all_insert * p.besseli_coeff_minus_B_insert *
		p.besseli_c_B_torque_insert;
}

bool isEmpty(string strInput)
{
	int nCount = 0;

	while (strInput[nCount])
	{
		if (!std::isspace(strInput[nCount])) return false;
		++nCount;
	}

	return true;
}

std::string trimWhitespace(string strInput)
{
	size_t nFirst = strInput.find_first_not_of(" \t");
	size_t nSecond = strInput.find_last_not_of(" \t");

	string strOutput = strInput.substr(nFirst, nSecond - nFirst + 1);

	return strOutput;
}

Params initialiseParams(Input &input)
{
	Params p;

	std::ifstream InputStream("input.dat");

	if (!InputStream)
		std::cout << "No input file (input.dat) found, using default values\n";

	// Update default values (created at the time of p creation) with input
	while (InputStream)
	{
		string strLine;
		getline(InputStream, strLine);

		// Ignore comment or empty lines
		if (!std::strncmp(strLine.c_str(), "#", 1) || isEmpty(strLine))
			continue;

		// Remove everything after #
		size_t nPos = strLine.find_first_of('#');
		if (nPos != string::npos)
			strLine.erase(nPos);

		assignInput(strLine, p);
	}

	p.a_B = p.A_B / p.b_B;
	p.c_B = p.C_B / p.b_B;
	p.q = 10.4 * p.b_B / 3.4;
	p.a_L = p.A_L / p.b_L;
	p.c_L = p.C_L / p.b_L;
	p.a_P = p.A_P / p.b_P;
	p.c_P = p.C_P / p.b_P;
	p.a_S = p.A_S / p.b_S;
	p.c_S = p.C_S / p.b_S;
	p.a_B_insert = p.A_B_insert / p.b_B;
	p.c_B_insert = p.C_B_insert / p.b_B;
	p.mu_L_plus = p.plus_coeff * p.mu_L;
	p.mu_L_minus = p.minus_coeff * p.mu_L;
	p.mu_P_plus = p.plus_coeff * p.mu_P;
	p.mu_P_minus = p.minus_coeff * p.mu_P;
	p.mu_S_plus = p.plus_coeff * p.mu_S;
	p.mu_S_minus = p.minus_coeff * p.mu_S;
	p.mu_L_insert_plus = p.plus_coeff * p.mu_L_insert;
	p.mu_L_insert_minus = p.minus_coeff * p.mu_L_insert;
	p.mu_P_insert_plus = p.plus_coeff * p.mu_P_insert;
	p.mu_P_insert_minus = p.minus_coeff * p.mu_P_insert;
	p.mu_S_insert_plus = p.plus_coeff * p.mu_S_insert;
	p.mu_S_insert_minus = p.minus_coeff * p.mu_S_insert;

	p.N = std::floor(input.DNA_length / p.b_B);
	p.N_insert = std::floor(input.insert_length / p.b_B);
	p.N_handle = std::floor((p.N - p.N_insert) / 2.0);
	p.N = 2 * p.N_handle + p.N_insert;  // Readjustment for consistency

	if (p.N_insert > p.N)
	{
		std::cerr << "Total DNA length is shorter than the length of the DNA insert\n";
		exit(2);
	}

	recalculateForceTorque(p, input.force, input.torque, input.max_mode);

	return p;
}

void assignInput(string strLine, Params &p)
{
	string strFirstInput, strSecondInput;

	size_t nPos = strLine.find_first_of('=');
	if (nPos == string::npos)
	{
		std::cerr << "The following line doesn't contain any assignment:" << std::endl;
		std::cerr << strLine << std::endl;
		exit(4);
	}
	else
	{
		strFirstInput = trimWhitespace(strLine.substr(0, nPos));
		strSecondInput = trimWhitespace(strLine.substr(nPos + 1));
	}

	if (!strFirstInput.compare("b_B")) p.b_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_B")) p.A_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_B")) p.C_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_B")) p.lambda_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("b_L")) p.b_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_L")) p.A_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_L")) p.C_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_L")) p.lambda_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_L")) p.mu_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_L_0")) p.lk_L_0 = std::stof(strSecondInput);
	if (!strFirstInput.compare("b_P")) p.b_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_P")) p.A_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_P")) p.C_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_P")) p.lambda_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_P")) p.mu_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_P_0")) p.lk_P_0 = std::stof(strSecondInput);
	if (!strFirstInput.compare("b_S")) p.b_S = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_S")) p.A_S = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_S")) p.C_S = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_S")) p.lambda_S = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_S")) p.mu_S = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_S_0")) p.lk_S_0 = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_B_insert")) p.A_B_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_B_insert")) p.C_B_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_B_insert")) p.lambda_B_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_L_insert")) p.mu_L_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_L_0_insert")) p.lk_L_0_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_P_insert")) p.mu_P_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_P_0_insert")) p.lk_P_0_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_S_insert")) p.mu_S_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_S_0_insert")) p.lk_S_0_insert = std::stof(strSecondInput);
	if (!strFirstInput.compare("coop")) p.coop = std::stof(strSecondInput);
	if (!strFirstInput.compare("cutoff")) p.cutoff = std::stof(strSecondInput);
	if (!strFirstInput.compare("plus_coeff")) p.plus_coeff = std::stof(strSecondInput);
	if (!strFirstInput.compare("minus_coeff")) p.minus_coeff = std::stof(strSecondInput);
}

vector2d loadForceTorqueFile(string filename)
{
	vector2d ftData;

	std::ifstream InputStream(filename.c_str());

	if (!InputStream)
	{
		std::cerr << "Couldn't open file " << filename << std::endl;
		exit(3);
	}

	while (InputStream)
	{
		string strLine;
		getline(InputStream, strLine);
		// Ignore comment or empty lines
		// Remove comments
		for (unsigned int nChar = 0; nChar < strLine.size(); ++nChar)
		{
			if (strLine[nChar] == '#')
			{
				strLine = strLine.substr(0, nChar);
				break;
			}
		}
		// Ignore empty lines
		if (std::all_of(strLine.begin(), strLine.end(), isspace))
			continue;

		std::stringstream StringStream;
		StringStream << strLine;

		vector<double> vdDataLine;
		while (!StringStream.eof())
		{
			string strValue;
			StringStream >> strValue;

			if (!strValue.empty())
			{
				double dValue = std::atof(strValue.c_str());
				vdDataLine.push_back(dValue);
			}
		}
		ftData.push_back(vdDataLine);
	}

	return ftData;
}