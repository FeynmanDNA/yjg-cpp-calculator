/*
 * A C++ implementation of the Matlab script for calculating DNA response
 * to force and torque constraints by Artem Yefremov
 * 
 * Implemented by Ladislav Hovan
 */

#include "main.h"

 // Params intentionally passed by value, although it might be more efficient to split the struct
void calculate(int count, vector2d const &ft_values, vector2d &vvdResults, int core, Input const &input, Params p,
	vector<double> &w3j_all, vector2d &L_1_0, vector2d &L_1_a_B, vector2d &L_1_a_L, vector2d &L_1_a_P, vector2d &L_1_a_S,
	vector2d &L_1_a_B_insert, MatrixXd &U, MatrixXd &V)
{
	vector<double> vdTemp(15);

	if (count > 0)
		recalculateForceTorque(p, ft_values[count][0], ft_values[count][1], input.max_mode);

	vdTemp[0] = ft_values[count][0]; 
	vdTemp[1] = ft_values[count][1];

	vector2d L_1_b_B_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_B * p.f, p.cutoff);
	vector2d L_1_b_B_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_B * p.f_plus, p.cutoff);
	vector2d L_1_b_B_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_B * p.f_minus, p.cutoff);
	//std::cout << "All L_k_r_b_B_f functions found\n";

	vector2d L_1_b_L_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_L * p.f, p.cutoff);
	vector2d L_1_b_L_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_L * p.f_plus, p.cutoff);
	vector2d L_1_b_L_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_L * p.f_minus, p.cutoff);
	//std::cout << "All L_k_r_b_L_f functions found\n";

	vector2d L_1_b_P_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_P * p.f, p.cutoff);
	vector2d L_1_b_P_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_P * p.f_plus, p.cutoff);
	vector2d L_1_b_P_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_P * p.f_minus, p.cutoff);
	//std::cout << "All L_k_r_b_P_f functions found\n\n";

	vector2d L_1_b_S_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_S * p.f, p.cutoff);
	vector2d L_1_b_S_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_S * p.f_plus, p.cutoff);
	vector2d L_1_b_S_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_S * p.f_minus, p.cutoff);
	//std::cout << "All L_k_r_b_S_f functions found\n\n";

	// ---------------------------Calculations start here---------------------------

	// ---------------------------DNA extension---------------------------

	vector<MatrixXd> S_BLPS_plus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B, L_1_a_B,
		L_1_b_B_f_plus, p.coeff_r_total_L, L_1_a_L, L_1_b_L_f_plus, p.coeff_r_total_P, L_1_a_P, L_1_b_P_f_plus,
		p.coeff_r_total_S, L_1_a_S, L_1_b_S_f_plus, p.coeff_r_total_B_insert, L_1_a_B_insert, w3j_all, input.max_mode);

	vector<MatrixXd> S_BLPS_minus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B, L_1_a_B,
		L_1_b_B_f_minus, p.coeff_r_total_L, L_1_a_L, L_1_b_L_f_minus, p.coeff_r_total_P, L_1_a_P, L_1_b_P_f_minus,
		p.coeff_r_total_S, L_1_a_S, L_1_b_S_f_minus, p.coeff_r_total_B_insert, L_1_a_B_insert, w3j_all, input.max_mode);

	MatrixXd S_plus_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS_plus, false);
	MatrixXd S_minus_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS_minus, false);

	double coeff_plus_handle_prod, coeff_minus_handle_prod;
	MatrixXd S_plus_handle_prod = quick_matrices_product_v3(S_plus_handle, p.N_handle - 1, coeff_plus_handle_prod);
	MatrixXd S_minus_handle_prod = quick_matrices_product_v3(S_minus_handle, p.N_handle - 1, coeff_minus_handle_prod);

	MatrixXd S_plus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS_plus, true);
	MatrixXd S_minus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS_minus, true);

	double coeff_plus_insert_prod, coeff_minus_insert_prod;
	MatrixXd S_plus_insert_prod = quick_matrices_product_v3(S_plus_insert, p.N_insert, coeff_plus_insert_prod);
	MatrixXd S_minus_insert_prod = quick_matrices_product_v3(S_minus_insert, p.N_insert, coeff_minus_insert_prod);

	MatrixXd O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f_plus, input.max_mode, V);
	MatrixXd O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f_minus, input.max_mode, V);

	complex<double> trace_plus = (U * S_plus_handle  * S_plus_handle_prod  * S_plus_insert_prod  * S_plus_handle_prod *
		O_plus).trace();
	complex<double> trace_minus = (U * S_minus_handle * S_minus_handle_prod * S_minus_insert_prod * S_minus_handle_prod *
		O_minus).trace();

	complex<double> ext = (2.0 * (coeff_plus_handle_prod - coeff_minus_handle_prod) + coeff_plus_insert_prod -
		coeff_minus_insert_prod + std::log(trace_plus) - std::log(trace_minus)) /
		(p.f_plus - p.f_minus) / input.DNA_length;
	double DNA_rel_extension = ext.real();
	double DNA_extension = DNA_rel_extension * input.DNA_length;

	vdTemp[2] = DNA_extension;
	vdTemp[3] = DNA_rel_extension;

	// ---------------------------L-DNA percentage---------------------------

	vector<MatrixXd> S_BLPS = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B, L_1_a_B, L_1_b_B_f,
		p.coeff_r_total_L, L_1_a_L, L_1_b_L_f, p.coeff_r_total_P, L_1_a_P, L_1_b_P_f, p.coeff_r_total_S, L_1_a_S,
		L_1_b_S_f, p.coeff_r_total_B_insert, L_1_a_B_insert, w3j_all, input.max_mode);

	S_plus_handle = create_transfer_matrix_torque_v2(p, p.mu_L_plus, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS, false);
	S_minus_handle = create_transfer_matrix_torque_v2(p, p.mu_L_minus, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS, false);
	MatrixXd S_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS, false);

	double coeff_handle_prod;
	S_plus_handle_prod = quick_matrices_product_v3(S_plus_handle, p.N_handle - 1, coeff_plus_handle_prod);
	S_minus_handle_prod = quick_matrices_product_v3(S_minus_handle, p.N_handle - 1, coeff_minus_handle_prod);
	MatrixXd S_handle_prod = quick_matrices_product_v3(S_handle, p.N_handle - 1, coeff_handle_prod);

	S_plus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert_plus, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);
	S_minus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert_minus, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);
	MatrixXd S_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);

	double coeff_insert_prod;
	S_plus_insert_prod = quick_matrices_product_v3(S_plus_insert, p.N_insert, coeff_plus_insert_prod);
	S_minus_insert_prod = quick_matrices_product_v3(S_minus_insert, p.N_insert, coeff_minus_insert_prod);
	MatrixXd S_insert_prod = quick_matrices_product_v3(S_insert, p.N_insert, coeff_insert_prod);

	O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L_plus, p.mu_P, p.mu_S, p.f, input.max_mode, V);
	O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L_minus, p.mu_P, p.mu_S, p.f, input.max_mode, V);
	MatrixXd O = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f, input.max_mode, V);

	complex<double> trace_handle_plus = (U * S_plus_handle * S_plus_handle_prod * S_insert_prod *
		S_plus_handle_prod * O_plus).trace();
	complex<double> trace_handle_minus = (U * S_minus_handle * S_minus_handle_prod * S_insert_prod *
		S_minus_handle_prod * O_minus).trace();

	complex<double> trace_insert_plus = (U * S_handle * S_handle_prod * S_plus_insert_prod * S_handle_prod * O).
		trace();
	complex<double> trace_insert_minus = (U * S_handle * S_handle_prod * S_minus_insert_prod * S_handle_prod * O).
		trace();

	complex<double> occ_handle = -(2.0 * (coeff_plus_handle_prod - coeff_minus_handle_prod) +
		std::log(trace_handle_plus) - std::log(trace_handle_minus)) /
		((p.mu_L_plus - p.mu_L_minus) * p.q * p.N);
	complex<double> occ_insert = -(coeff_plus_insert_prod - coeff_minus_insert_prod + std::log(trace_insert_plus) -
		std::log(trace_insert_minus)) / ((p.mu_L_insert_plus - p.mu_L_insert_minus) * p.q * p.N);

	double DNA_L_handle_occupancy = std::abs(occ_handle.real());
	double DNA_L_insert_occupancy = std::abs(occ_insert.real());
	double DNA_L_occupancy = DNA_L_handle_occupancy + DNA_L_insert_occupancy;

	vdTemp[4] = DNA_L_occupancy;
	vdTemp[5] = DNA_L_handle_occupancy * p.N / 2.0 / p.N_handle;
	vdTemp[6] = DNA_L_insert_occupancy * p.N / p.N_insert;

	// ---------------------------P-DNA percentage---------------------------

	S_plus_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P_plus, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS, false);
	S_minus_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P_minus, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS, false);
	S_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0, input.max_mode,
		S_BLPS, false);

	S_plus_handle_prod = quick_matrices_product_v3(S_plus_handle, p.N_handle - 1, coeff_plus_handle_prod);
	S_minus_handle_prod = quick_matrices_product_v3(S_minus_handle, p.N_handle - 1, coeff_minus_handle_prod);
	S_handle_prod = quick_matrices_product_v3(S_handle, p.N_handle - 1, coeff_handle_prod);

	S_plus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert_plus, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);
	S_minus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert_minus, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);
	S_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert, p.lk_L_0_insert,
		p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);

	S_plus_insert_prod = quick_matrices_product_v3(S_plus_insert, p.N_insert, coeff_plus_insert_prod);
	S_minus_insert_prod = quick_matrices_product_v3(S_minus_insert, p.N_insert, coeff_minus_insert_prod);
	S_insert_prod = quick_matrices_product_v3(S_insert, p.N_insert, coeff_insert_prod);

	O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P_plus, p.mu_S, p.f, input.max_mode, V);
	O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P_minus, p.mu_S, p.f, input.max_mode, V);
	O = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f, input.max_mode, V);

	trace_handle_plus = (U * S_plus_handle * S_plus_handle_prod * S_insert_prod * S_plus_handle_prod * O_plus).trace();
	trace_handle_minus = (U * S_minus_handle * S_minus_handle_prod * S_insert_prod * S_minus_handle_prod * O_minus).
		trace();

	trace_insert_plus = (U * S_handle * S_handle_prod * S_plus_insert_prod * S_handle_prod * O).trace();
	trace_insert_minus = (U * S_handle * S_handle_prod * S_minus_insert_prod * S_handle_prod * O).trace();

	occ_handle = -(2.0 * (coeff_plus_handle_prod - coeff_minus_handle_prod) + std::log(trace_handle_plus) -
		std::log(trace_handle_minus)) / ((p.mu_P_plus - p.mu_P_minus) * p.q * p.N);
	occ_insert = -(coeff_plus_insert_prod - coeff_minus_insert_prod + std::log(trace_insert_plus) -
		std::log(trace_insert_minus)) / ((p.mu_P_insert_plus - p.mu_P_insert_minus) * p.q * p.N);

	double DNA_P_handle_occupancy = std::abs(occ_handle.real());
	double DNA_P_insert_occupancy = std::abs(occ_insert.real());
	double DNA_P_occupancy = DNA_P_handle_occupancy + DNA_P_insert_occupancy;

	vdTemp[7] = DNA_P_occupancy;
	vdTemp[8] = DNA_P_handle_occupancy * p.N / 2.0 / p.N_handle;
	vdTemp[9] = DNA_P_insert_occupancy * p.N / p.N_insert;

	// ---------------------------S-DNA percentage---------------------------

	S_plus_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S_plus, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS, false);
	S_minus_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S_minus, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, S_BLPS, false);
	S_handle = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0, input.max_mode,
		S_BLPS, false);

	S_plus_handle_prod = quick_matrices_product_v3(S_plus_handle, p.N_handle - 1, coeff_plus_handle_prod);
	S_minus_handle_prod = quick_matrices_product_v3(S_minus_handle, p.N_handle - 1, coeff_minus_handle_prod);
	S_handle_prod = quick_matrices_product_v3(S_handle, p.N_handle - 1, coeff_handle_prod);

	S_plus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert_plus,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);
	S_minus_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert_minus,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);
	S_insert = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert, p.lk_L_0_insert,
		p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode, S_BLPS, true);

	S_plus_insert_prod = quick_matrices_product_v3(S_plus_insert, p.N_insert, coeff_plus_insert_prod);
	S_minus_insert_prod = quick_matrices_product_v3(S_minus_insert, p.N_insert, coeff_minus_insert_prod);
	S_insert_prod = quick_matrices_product_v3(S_insert, p.N_insert, coeff_insert_prod);

	O_plus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S_plus, p.f, input.max_mode, V);
	O_minus = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S_minus, p.f, input.max_mode, V);
	O = create_second_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.mu_S, p.f, input.max_mode, V);

	trace_handle_plus = (U * S_plus_handle * S_plus_handle_prod * S_insert_prod * S_plus_handle_prod * O_plus).
		trace();
	trace_handle_minus = (U * S_minus_handle * S_minus_handle_prod * S_insert_prod * S_minus_handle_prod * O_minus).
		trace();

	trace_insert_plus = (U * S_handle * S_handle_prod * S_plus_insert_prod * S_handle_prod * O).trace();
	trace_insert_minus = (U * S_handle * S_handle_prod * S_minus_insert_prod * S_handle_prod * O).trace();

	occ_handle = -(2.0 * (coeff_plus_handle_prod - coeff_minus_handle_prod) + std::log(trace_handle_plus) -
		std::log(trace_handle_minus)) / ((p.mu_S_plus - p.mu_S_minus) * p.q * p.N);
	occ_insert = -(coeff_plus_insert_prod - coeff_minus_insert_prod + std::log(trace_insert_plus) -
		std::log(trace_insert_minus)) / ((p.mu_S_insert_plus - p.mu_S_insert_minus) * p.q * p.N);

	double DNA_S_handle_occupancy = std::abs(occ_handle.real());
	double DNA_S_insert_occupancy = std::abs(occ_insert.real());
	double DNA_S_occupancy = DNA_S_handle_occupancy + DNA_S_insert_occupancy;

	vdTemp[10] = DNA_S_occupancy;
	vdTemp[11] = DNA_S_handle_occupancy * p.N / 2.0 / p.N_handle;
	vdTemp[12] = DNA_S_insert_occupancy * p.N / p.N_insert;

	// ---------------------------DNA linking number change---------------------------

	S_BLPS_plus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B_plus, L_1_a_B, L_1_b_B_f,
		p.coeff_r_total_L_plus, L_1_a_L, L_1_b_L_f, p.coeff_r_total_P_plus, L_1_a_P, L_1_b_P_f, p.coeff_r_total_S_plus,
		L_1_a_S, L_1_b_S_f, p.coeff_r_total_B_plus_insert, L_1_a_B_insert, w3j_all, input.max_mode);
	S_BLPS_minus = create_matrix_block_SB_SL_SP_SS_torque_v2(L_1_0, p.coeff_r_total_B_minus, L_1_a_B, L_1_b_B_f,
		p.coeff_r_total_L_minus, L_1_a_L, L_1_b_L_f, p.coeff_r_total_P_minus, L_1_a_P, L_1_b_P_f, p.coeff_r_total_S_minus,
		L_1_a_S, L_1_b_S_f, p.coeff_r_total_B_minus_insert, L_1_a_B_insert, w3j_all, input.max_mode);

	MatrixXd S_plus_a = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS_plus[0], S_BLPS[1], S_BLPS[2], S_BLPS[3], S_BLPS[4] }, false);
	MatrixXd S_plus_b = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS[0], S_BLPS_plus[1], S_BLPS[2], S_BLPS[3], S_BLPS[4] }, false);
	MatrixXd S_plus_c = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS[0], S_BLPS[1], S_BLPS_plus[2], S_BLPS[3], S_BLPS[4] }, false);
	MatrixXd S_plus_d = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS_plus[3], S_BLPS[4] }, false);

	MatrixXd S_minus_a = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS_minus[0], S_BLPS[1], S_BLPS[2], S_BLPS[3], S_BLPS[4] }, false);
	MatrixXd S_minus_b = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS[0], S_BLPS_minus[1], S_BLPS[2], S_BLPS[3], S_BLPS[4] }, false);
	MatrixXd S_minus_c = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS[0], S_BLPS[1], S_BLPS_minus[2], S_BLPS[3], S_BLPS[4] }, false);
	MatrixXd S_minus_d = create_transfer_matrix_torque_v2(p, p.mu_L, p.mu_P, p.mu_S, p.lk_L_0, p.lk_P_0, p.lk_S_0,
		input.max_mode, { S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS_minus[3], S_BLPS[4] }, false);

	double coeff_plus_prod_a, coeff_plus_prod_b, coeff_plus_prod_c, coeff_plus_prod_d;
	MatrixXd S_plus_prod_a = quick_matrices_product_v3(S_plus_a, p.N_handle - 1, coeff_plus_prod_a);
	MatrixXd S_plus_prod_b = quick_matrices_product_v3(S_plus_b, p.N_handle - 1, coeff_plus_prod_b);
	MatrixXd S_plus_prod_c = quick_matrices_product_v3(S_plus_c, p.N_handle - 1, coeff_plus_prod_c);
	MatrixXd S_plus_prod_d = quick_matrices_product_v3(S_plus_d, p.N_handle - 1, coeff_plus_prod_d);

	double coeff_minus_prod_a, coeff_minus_prod_b, coeff_minus_prod_c, coeff_minus_prod_d;
	MatrixXd S_minus_prod_a = quick_matrices_product_v3(S_minus_a, p.N_handle - 1, coeff_minus_prod_a);
	MatrixXd S_minus_prod_b = quick_matrices_product_v3(S_minus_b, p.N_handle - 1, coeff_minus_prod_b);
	MatrixXd S_minus_prod_c = quick_matrices_product_v3(S_minus_c, p.N_handle - 1, coeff_minus_prod_c);
	MatrixXd S_minus_prod_d = quick_matrices_product_v3(S_minus_d, p.N_handle - 1, coeff_minus_prod_d);


	MatrixXd S_plus_a_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS[3], S_BLPS_plus[4] }, true);
	MatrixXd S_plus_b_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS_plus[1], S_BLPS[2], S_BLPS[3], S_BLPS[4] }, true);
	MatrixXd S_plus_c_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS_plus[2], S_BLPS[3], S_BLPS[4] }, true);
	MatrixXd S_plus_d_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS_plus[3], S_BLPS[4] }, true);

	MatrixXd S_minus_a_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS[3], S_BLPS_minus[4] }, true);
	MatrixXd S_minus_b_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS_minus[1], S_BLPS[2], S_BLPS[3], S_BLPS[4] }, true);
	MatrixXd S_minus_c_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS_minus[2], S_BLPS[3], S_BLPS[4] }, true);
	MatrixXd S_minus_d_ins = create_transfer_matrix_torque_v2(p, p.mu_L_insert, p.mu_P_insert, p.mu_S_insert,
		p.lk_L_0_insert, p.lk_P_0_insert, p.lk_S_0_insert, input.max_mode,
		{ S_BLPS[0], S_BLPS[1], S_BLPS[2], S_BLPS_minus[3], S_BLPS[4] }, true);

	double coeff_plus_prod_a_ins, coeff_plus_prod_b_ins, coeff_plus_prod_c_ins, coeff_plus_prod_d_ins;
	MatrixXd S_plus_prod_a_ins = quick_matrices_product_v3(S_plus_a_ins, p.N_insert, coeff_plus_prod_a_ins);
	MatrixXd S_plus_prod_b_ins = quick_matrices_product_v3(S_plus_b_ins, p.N_insert, coeff_plus_prod_b_ins);
	MatrixXd S_plus_prod_c_ins = quick_matrices_product_v3(S_plus_c_ins, p.N_insert, coeff_plus_prod_c_ins);
	MatrixXd S_plus_prod_d_ins = quick_matrices_product_v3(S_plus_d_ins, p.N_insert, coeff_plus_prod_d_ins);

	double coeff_minus_prod_a_ins, coeff_minus_prod_b_ins, coeff_minus_prod_c_ins, coeff_minus_prod_d_ins;
	MatrixXd S_minus_prod_a_ins = quick_matrices_product_v3(S_minus_a_ins, p.N_insert, coeff_minus_prod_a_ins);
	MatrixXd S_minus_prod_b_ins = quick_matrices_product_v3(S_minus_b_ins, p.N_insert, coeff_minus_prod_b_ins);
	MatrixXd S_minus_prod_c_ins = quick_matrices_product_v3(S_minus_c_ins, p.N_insert, coeff_minus_prod_c_ins);
	MatrixXd S_minus_prod_d_ins = quick_matrices_product_v3(S_minus_d_ins, p.N_insert, coeff_minus_prod_d_ins);


	complex<double> trace_plus_a = (U * S_plus_a * S_plus_prod_a * S_plus_prod_a_ins * S_plus_prod_a * O).trace();
	complex<double> trace_plus_b = (U * S_plus_b * S_plus_prod_b * S_plus_prod_b_ins * S_plus_prod_b * O).trace();
	complex<double> trace_plus_c = (U * S_plus_c * S_plus_prod_c * S_plus_prod_c_ins * S_plus_prod_c * O).trace();
	complex<double> trace_plus_d = (U * S_plus_d * S_plus_prod_d * S_plus_prod_d_ins * S_plus_prod_d * O).trace();

	complex<double> trace_minus_a = (U * S_minus_a * S_minus_prod_a * S_minus_prod_a_ins * S_minus_prod_a * O).trace();
	complex<double> trace_minus_b = (U * S_minus_b * S_minus_prod_b * S_minus_prod_b_ins * S_minus_prod_b * O).trace();
	complex<double> trace_minus_c = (U * S_minus_c * S_minus_prod_c * S_minus_prod_c_ins * S_minus_prod_c * O).trace();
	complex<double> trace_minus_d = (U * S_minus_d * S_minus_prod_d * S_minus_prod_d_ins * S_minus_prod_d * O).trace();


	complex<double> linking_number_a = (2.0 * (coeff_plus_prod_a - coeff_minus_prod_a) + coeff_plus_prod_a_ins -
		coeff_minus_prod_a_ins + std::log(trace_plus_a) - std::log(trace_minus_a)) /
		((p.B_coeff_plus - p.B_coeff_minus) * 2 * pi);
	complex<double> linking_number_b = (2.0 * (coeff_plus_prod_b - coeff_minus_prod_b) + coeff_plus_prod_b_ins -
		coeff_minus_prod_b_ins + std::log(trace_plus_b) - std::log(trace_minus_b)) /
		((p.L_coeff_plus - p.L_coeff_minus) * 2 * pi);
	complex<double> linking_number_c = (2.0 * (coeff_plus_prod_c - coeff_minus_prod_c) + coeff_plus_prod_c_ins -
		coeff_minus_prod_c_ins + std::log(trace_plus_c) - std::log(trace_minus_c)) /
		((p.P_coeff_plus - p.P_coeff_minus) * 2 * pi);
	complex<double> linking_number_d = (2.0 * (coeff_plus_prod_d - coeff_minus_prod_d) + coeff_plus_prod_d_ins -
		coeff_minus_prod_d_ins + std::log(trace_plus_d) - std::log(trace_minus_d)) /
		((p.S_coeff_plus - p.S_coeff_minus) * 2 * pi);

	complex<double> linking_number = linking_number_a + linking_number_b + linking_number_c + linking_number_d + p.q *
		p.N * (p.lk_L_0 * DNA_L_handle_occupancy + p.lk_P_0 * DNA_P_handle_occupancy + p.lk_S_0 *
			DNA_S_handle_occupancy + p.lk_L_0_insert * DNA_L_insert_occupancy + p.lk_P_0_insert * DNA_P_insert_occupancy +
			p.lk_S_0_insert * DNA_S_insert_occupancy);

	double DNA_linking = real(linking_number);
	double DNA_superhelical_density = DNA_linking / (input.DNA_length / 3.4);

	vdTemp[13] = DNA_linking;
	vdTemp[14] = DNA_superhelical_density;

	std::swap(vvdResults[core], vdTemp);
}

int main(int argc, char *argv[])
{
	Timer global;

	vector2d ft_values;
	Input input;

	// Input
	if (argc == 6)
	{
		ft_values = loadForceTorqueFile(argv[3]);
		input.max_mode = std::atoi(argv[4]);
		input.n_threads = std::atoi(argv[5]);
	}
	else if (argc == 7)
	{
		ft_values.push_back({ std::atof(argv[3]), std::atof(argv[4]) });
		input.max_mode = std::atoi(argv[5]);
		input.n_threads = std::atoi(argv[6]);
	}
	else
	{
		std::cerr << "The program requires one of two input styles:\n";
		std::cerr << "1. DNA length, insert length, force, torque, number of modes and number of threads (6 inputs)\n";
		std::cerr << "2. DNA length, insert length, name of file containing force and torque values," 
			<< " number of modes and number of threads (5 inputs)\n";
		exit(2);
	}

	input.DNA_length = std::atof(argv[1]);
	input.insert_length = std::atof(argv[2]);

	// Calculating parametres from given input
	input.force = ft_values[0][0];
	input.torque = ft_values[0][1];
	Params p = initialiseParams(input);

	vector<double> w3j_all = all_Wigner_3j_symbols_torque(input.max_mode - 1);
	//std::cout << "All w3j symbols found\n";

	vector2d L_1_0 = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, 0, p.cutoff);
	vector2d L_1_a_B = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_B, p.cutoff);
	vector2d L_1_a_B_insert = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_B_insert, p.cutoff);
	//std::cout << "All L_k_r_B and L_k_r_a_B functions found\n";

	vector2d L_1_a_L = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_L, p.cutoff);
	//std::cout << "All L_k_r_a_L functions found\n";

	vector2d L_1_a_P = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_P, p.cutoff);
	//std::cout << "All L_k_r_a_P functions found\n";

	vector2d L_1_a_S = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_S, p.cutoff);
	//std::cout << "All L_k_r_a_S functions found\n";

	MatrixXd U = create_first_boundary_matrix_v1(input.max_mode);
	MatrixXd V = create_matrix_V(input.max_mode);

	// Output file
	std::ofstream Output;
	if (argc == 6)
	{
		Output.open("output.csv");
		Output << "# NUS Yan Jie Group @ 2018\n";
		Output << "# force, torque, extension, rel_extension, L_occupancy, L_occ_handle, L_occ_insert, " <<
			"P_occupancy, P_occ_handle, P_occ_insert, S_occupancy, S_occ_handle, S_occ_insert, linking, superhelical\n";
	}

	int count = 0;
	bool has_negative = false;
	while (count < ft_values.size())
	{
		vector2d vvdMetaResults;
		vector<thread> vtThreads;
		for (int core = 0; core < input.n_threads; ++core)
		{
			vvdMetaResults.emplace_back();
			thread temp(calculate, count, std::ref(ft_values), std::ref(vvdMetaResults), core, std::ref(input),
				p, std::ref(w3j_all), std::ref(L_1_0), std::ref(L_1_a_B), std::ref(L_1_a_L), std::ref(L_1_a_P),
				std::ref(L_1_a_S), std::ref(L_1_a_B_insert), std::ref(U), std::ref(V));
			vtThreads.push_back(thread(std::move(temp)));
			++count;
			if (count == ft_values.size())
				break;
		}
		for (auto &thread : vtThreads)
			thread.join();
		for (auto &values : vvdMetaResults)
		{
			if (Output)
			{
				for (int nPos = 0; nPos < values.size(); ++nPos)
				{
					Output << values[nPos];
					// Extension
					if (nPos == 2 && values[nPos] < 0.0)
						has_negative = true;
					if (nPos < values.size() - 1)
						Output << ",";
				}
				Output << "\n";
			}
		}
	}

	if (Output)
	{
		if (has_negative)
			Output << "# The file contains negative extension values\n" <<
				"# These result from the DNA being in supercoiled state, which our formula doesn't cover\n" <<
				"# Effective extension for these cases can be considered zero\n";
		Output.close();
	}
	double runtime = global.elapsed();
	std::cout << "Total t: " << runtime << "s\n";
	if (ft_values.size() > 1)
		std::cout << "Average t / calculation: " << runtime / ft_values.size() << "s\n";

	return 0;
}