#include "functions.h"

valarray<double> find_all_Besseli_r(double x, int max_n)
{
	valarray<double> varray(max_n + 1);

	for (int i = 0; i <= max_n; ++i)
		varray[i] = boost::math::cyl_bessel_i(i, x);

	return varray;
}

int index(int i1, int i2, int i3, int i4, int max_n)
{
	int idx = i4 + (2 * max_n + 1) * i3 + (2 * max_n + 1) * (max_n + 1) * i2
		+ (2 * max_n + 1) * (2 * max_n + 2) * (max_n + 1) * i1;

	return idx;
}

vector<double> all_Wigner_3j_symbols_torque(int max_n)
{
	int arr_size = std::pow(max_n + 1, 2) * (2 * max_n + 1) * (2 * max_n + 2);
	vector<double> w3j_all(arr_size, 0.0);

	for (int i = 0; i <= max_n; ++i)
	{
		for (int j = 0; j <= 2 * max_n + 1; ++j)
		{
			for (int k = 0; k <= max_n; ++k)
			{
				for (int r = -std::min(i, j); r <= std::min(i, j); ++r)
				{
					if (r <= 0)
						w3j_all[index(i, j, k, max_n + r, max_n)] = 
							std::pow(WignerSymbols::wigner3j(i, j, k, -r, r, 0), 2);
					else
						w3j_all[index(i, j, k, max_n + r, max_n)] = 
							w3j_all[index(i, j, k, max_n - r, max_n)];
				}
			}
		}
	}

	return w3j_all;
}

long double factorial(int n)
{
	return (n == 1 || n == 0) ? 1 : n * factorial(n - 1);
}

double j_polynomial_my(double n, double alpha, double beta, double x)
{
	// LH: This function is modified, assumes only one evaluation point

	if (alpha <= -1.0)
		std::cerr << "We require alpha to be > -1\n";
	if (beta <= -1.0)
		std::cerr << "We require beta to be > -1\n";

	vector<double> v(n + 1, 1.0);

	if (n == 0)
		return 1.0;

	v[1] = (1.0 + 0.5 * (alpha + beta)) * x + 0.5 * (alpha - beta);

	for (int i = 2; i <= n; ++i)
	{
		double c1 = 2 * i * (i + alpha + beta) * (2 * i - 2 + alpha + beta);
		double c2 = (2 * i - 1 + alpha + beta) * (2 * i + alpha + beta) * (2 * i - 2 + alpha + beta);
		double c3 = (2 * i - 1 + alpha + beta) * (alpha + beta) * (alpha - beta);
		double c4 = -2 * (i - 1 + alpha) * (i - 1 + beta)  * (2 * i + alpha + beta);

		v[i] = ((c3 + c2 * x) * v[i - 1] + c4 * v[i - 2]) / c1;
	}

	double y = v.back();

	return y;
}

double P_n_ml_v2(double n, double m, double l, double theta)
{
	// program calculates P_n_ml polynomials

	// check conditions
	if (n < 0 || std::fmod(2 * n, 1) != 0 || std::fmod(2 * m, 1) != 0 || std::fmod(2 * l, 1) != 0 ||
		std::abs(m) > n || std::abs(l) > n || std::fmod(n + m, 1) != 0 || std::fmod(n + l, 1) != 0)
		std::cerr << "P_n_ml polynomials error\n";

	double a = std::abs(m - l);
	double b = std::abs(m + l);
	double k = n - (a + b) / 2;
	double lam = std::max(0.0, m - l);

	double cos_theta = std::cos(theta);
	double cos_theta_2 = std::cos(theta / 2);
	double sin_theta_2 = std::sin(theta / 2);

	double fact_result = factorial(k) * factorial(k + a + b) / 
		static_cast<long double>(factorial(k + a) * factorial(k + b));

	complex<double> p = std::pow(1i, m - l) * std::pow(-1, lam) * std::sqrt(fact_result) * 
		std::pow(sin_theta_2, a) * std::pow(cos_theta_2, b) * j_polynomial_my(k, a, b, cos_theta);

	// LH: Assume p is real
	return p.real();
}

double K_function(double x, double n, double l, double a)
{
	return std::exp(-a * (std::cos(x) - 1)) * P_n_ml_v2(n, l, l, x) * std::sin(x);
}

double L_k_r_v3(double n, double l, double a, double cutoff)
{
	if (n < 0 || std::abs(l) > n || std::fmod(n, 1) != 0 || std::fmod(l, 1) != 0)
		std::cerr << "K_n_l coefficients error\n";
	
	auto kfunc = [n, l, a](double x) { return K_function(x, n, l, a); };
	double coefficient = std::exp(-a) * 
		boost::math::quadrature::gauss_kronrod<double, 61>::integrate(kfunc, 0.0, pi, 0, 0);  
	// full sphere surface : cutoff = pi

	return coefficient;
}

vector2d find_indexed_L_k_r_v2(int max_n, int max_l, double a, double cutoff)
{
	vector2d K(max_n + 1);

	for (int p = 0; p <= max_n; ++p)
	{
		K[p] = vector<double>(2 * max_l + 1, 0.0);

		for (int q = std::min(p, max_l); q >= -std::min(p, max_l); --q)
		{
			if (q >= 0)
				K[p][max_l + q] = L_k_r_v3(p, q, a, cutoff);
			else
				K[p][max_l + q] = K[p][max_l - q];
		}
	}

	return K;
}