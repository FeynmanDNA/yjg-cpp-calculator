#include "main.h"

void calculate(int count, vector2d const &ft_values, vector2d &vvdResults, int core, Input const &input, Params p,
	vector<double> &w3j_all, vector2d &L_0, vector2d &L_a_B, vector2d &L_a_L, vector2d &L_a_P, vector2d &L_a_protein,
	vectorcd &D_n_ml_A_in, vectorcd &D_n_ml_A_out, MatrixXd &U, MatrixXd &V)
{
	vector<double> vdTemp(9);
	
	if (count > 0)
		recalculateForceTorque(p, ft_values[count][0], ft_values[count][1], input.mu_protein, input.max_mode);

	vdTemp[0] = ft_values[count][0]; 
	vdTemp[1] = ft_values[count][1];

	vector2d L_b_B_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_B * p.f, p.cutoff);
	vector2d L_b_B_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_B * p.f_plus, p.cutoff);
	vector2d L_b_B_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_B * p.f_minus, p.cutoff);
	//std::cout << "All L_b_B_f functions found\n";

	vector2d L_b_L_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_L * p.f, p.cutoff);
	vector2d L_b_L_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_L * p.f_plus, p.cutoff);
	vector2d L_b_L_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_L * p.f_minus, p.cutoff);
	//std::cout << "All L_b_L_f functions found\n";

	vector2d L_b_P_f = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, -p.b_P * p.f, p.cutoff);
	vector2d L_b_P_f_plus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_P * p.f_plus, p.cutoff);
	vector2d L_b_P_f_minus = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1,
		-p.b_P * p.f_minus, p.cutoff);
	//std::cout << "All L_b_P_f functions found\n\n";

	vector<MatrixXcd> S_BLP_full = create_matrix_block_SB_SL_SP_Sprotein_v10_nucleosome(p, input.max_mode,
		w3j_all, L_0, L_a_B, L_b_B_f, L_b_B_f_plus, L_b_B_f_minus, L_a_L, L_b_L_f, L_b_L_f_plus, L_b_L_f_minus,
		L_a_P, L_b_P_f, L_b_P_f_plus, L_b_P_f_minus, L_a_protein, D_n_ml_A_in, D_n_ml_A_out);

	// DNA Extension

	MatrixXcd S_protein_plus_f = create_protein_matrix_v1_nucleosome(S_BLP_full[S_protein_plus_A_in_f],
		S_BLP_full[S_protein_plus_A_out_f], S_BLP_full[S_protein_plus_B_f], p.protein_size, input.max_mode);
	MatrixXcd S_protein_minus_f = create_protein_matrix_v1_nucleosome(S_BLP_full[S_protein_minus_A_in_f],
		S_BLP_full[S_protein_minus_A_out_f], S_BLP_full[S_protein_minus_B_f], p.protein_size, input.max_mode);

	MatrixXcd S_plus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B_plus_f], S_BLP_full[S_L_plus_f],
		S_BLP_full[S_P_plus_f], S_protein_plus_f, input.max_mode, p.energy_coeff);
	MatrixXcd S_minus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B_minus_f], S_BLP_full[S_L_minus_f],
		S_BLP_full[S_P_minus_f], S_protein_minus_f, input.max_mode, p.energy_coeff);

	MatrixXd O_plus = create_first_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.f_plus, input.max_mode,
		p.L + 3, V);
	MatrixXd O_minus = create_first_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.f_minus, input.max_mode,
		p.L + 3, V);

	complex<double> coeff_plus_prod, coeff_minus_prod;
	MatrixXcd S_plus_prod = quick_matrices_product_v3(S_plus, p.power_num, coeff_plus_prod);
	MatrixXcd S_minus_prod = quick_matrices_product_v3(S_minus, p.power_num, coeff_minus_prod);

	complex<double> trace_plus = (U * S_plus_prod * O_plus).trace();
	complex<double> trace_minus = (U * S_minus_prod * O_minus).trace();

	complex<double> ext = (coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus)) /
		((p.f_plus - p.f_minus) * (p.N + 1) * p.b_B);
	double DNA_rel_extension = ext.real();
	double DNA_extension = DNA_rel_extension * input.DNA_length;

	vdTemp[2] = DNA_extension; 
	vdTemp[3] = DNA_rel_extension;

	// L-DNA occupancy

	double plus_coeff = std::exp(-p.q * (p.mu_L_plus - p.mu_L));
	double minus_coeff = std::exp(-p.q * (p.mu_L_minus - p.mu_L));

	MatrixXcd S_protein = create_protein_matrix_v1_nucleosome(S_BLP_full[S_protein_A_in],
		S_BLP_full[S_protein_A_out], S_BLP_full[S_protein_B], p.protein_size, input.max_mode);

	S_plus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], plus_coeff * S_BLP_full[S_L],
		S_BLP_full[S_P], S_protein, input.max_mode, p.energy_coeff);
	S_minus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], minus_coeff * S_BLP_full[S_L],
		S_BLP_full[S_P], S_protein, input.max_mode, p.energy_coeff);

	O_plus = create_first_boundary_matrix_torque_v3(p, p.mu_L_plus, p.mu_P, p.f, input.max_mode, p.L + 3, V);
	O_minus = create_first_boundary_matrix_torque_v3(p, p.mu_L_minus, p.mu_P, p.f, input.max_mode, p.L + 3, V);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.power_num, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.power_num, coeff_minus_prod);

	trace_plus = (U * S_plus_prod * O_plus).trace();
	trace_minus = (U * S_minus_prod * O_minus).trace();

	complex<double> occ = -(coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus))
		/ ((p.mu_L_plus - p.mu_L_minus) * p.q * (p.N + 1));
	double DNA_L_occupancy = std::abs(occ.real());

	vdTemp[4] = DNA_L_occupancy;

	// P-DNA occupancy

	plus_coeff = std::exp(-p.q * (p.mu_L_plus - p.mu_L));
	minus_coeff = std::exp(-p.q * (p.mu_L_minus - p.mu_L));

	S_plus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L], plus_coeff *
		S_BLP_full[S_P], S_protein, input.max_mode, p.energy_coeff);
	S_minus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L], minus_coeff *
		S_BLP_full[S_P], S_protein, input.max_mode, p.energy_coeff);

	O_plus = create_first_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P_plus, p.f, input.max_mode, p.L + 3, V);
	O_minus = create_first_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P_minus, p.f, input.max_mode, p.L + 3, V);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.power_num, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.power_num, coeff_minus_prod);

	trace_plus = (U * S_plus_prod * O_plus).trace();
	trace_minus = (U * S_minus_prod * O_minus).trace();

	occ = -(coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus)) /
		((p.mu_P_plus - p.mu_P_minus) * p.q * (p.N + 1));
	double DNA_P_occupancy = std::abs(occ.real());

	vdTemp[5] = DNA_P_occupancy;

	// pr-DNA occupancy

	plus_coeff = std::exp((p.mu_protein_plus - input.mu_protein) * p.block_size / p.protein_size);
	minus_coeff = std::exp((p.mu_protein_minus - input.mu_protein) * p.block_size / p.protein_size);

	S_plus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L], S_BLP_full[S_P],
		S_protein, input.max_mode, p.energy_coeff * plus_coeff);
	S_minus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L], S_BLP_full[S_P],
		S_protein, input.max_mode, p.energy_coeff * minus_coeff);

	MatrixXd O = create_first_boundary_matrix_torque_v3(p, p.mu_L, p.mu_P, p.f, input.max_mode, p.L + 3, V);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.power_num, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.power_num, coeff_minus_prod);

	trace_plus = (U * S_plus_prod * O).trace();
	trace_minus = (U * S_minus_prod * O).trace();

	occ = p.protein_size * (coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) - std::log(trace_minus)) /
		((p.mu_protein_plus - p.mu_protein_minus) * (p.N + 1));
	double DNA_pr_occupancy = std::abs(occ.real());

	vdTemp[6] = DNA_pr_occupancy;

	// Linking number difference

	// B-DNA part

	S_plus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B_plus_t], S_BLP_full[S_L], S_BLP_full[S_P],
		S_protein, input.max_mode, p.energy_coeff);
	S_minus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B_minus_t], S_BLP_full[S_L], S_BLP_full[S_P],
		S_protein, input.max_mode, p.energy_coeff);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.power_num, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.power_num, coeff_minus_prod);

	trace_plus = (U * S_plus_prod * O).trace();
	trace_minus = (U * S_minus_prod * O).trace();

	complex<double> linking_number_B = (coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) -
		std::log(trace_minus)) / ((p.B_coeff_plus - p.B_coeff_minus) * 2 * pi);

	// L-DNA part

	S_plus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L_plus_t], S_BLP_full[S_P],
		S_protein, input.max_mode, p.energy_coeff);
	S_minus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L_minus_t], S_BLP_full[S_P],
		S_protein, input.max_mode, p.energy_coeff);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.power_num, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.power_num, coeff_minus_prod);

	trace_plus = (U * S_plus_prod * O).trace();
	trace_minus = (U * S_minus_prod * O).trace();

	complex<double> linking_number_L = (coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) -
		std::log(trace_minus)) / ((p.L_coeff_plus - p.L_coeff_minus) * 2 * pi);

	// P-DNA part

	S_plus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L], S_BLP_full[S_P_plus_t],
		S_protein, input.max_mode, p.energy_coeff);
	S_minus = create_transfer_matrix_nucleosome_v7(p, S_BLP_full[S_B], S_BLP_full[S_L], S_BLP_full[S_P_minus_t],
		S_protein, input.max_mode, p.energy_coeff);

	S_plus_prod = quick_matrices_product_v3(S_plus, p.power_num, coeff_plus_prod);
	S_minus_prod = quick_matrices_product_v3(S_minus, p.power_num, coeff_minus_prod);

	trace_plus = (U * S_plus_prod * O).trace();
	trace_minus = (U * S_minus_prod * O).trace();

	complex<double> linking_number_P = (coeff_plus_prod - coeff_minus_prod + std::log(trace_plus) -
		std::log(trace_minus)) / ((p.P_coeff_plus - p.P_coeff_minus) * 2 * pi);

	// Final part

	complex<double> linking_number = linking_number_B + linking_number_L + linking_number_P +
		p.lk_L_0 * p.q * (p.N + 1) * DNA_L_occupancy + p.lk_P_0 * p.q * (p.N + 1) * DNA_P_occupancy +
		p.Lk_protein * (p.N + 1) * DNA_pr_occupancy / p.protein_size;
	double DNA_linking = real(linking_number);
	double DNA_superhelical_density = DNA_linking / (input.DNA_length / 3.4);

	vdTemp[7] = DNA_linking; 
	vdTemp[8] = DNA_superhelical_density;

	std::swap(vvdResults[core], vdTemp);
}

int main(int argc, char *argv[])
{
	Timer global;

	vector2d ft_values;
	Input input;

	// Input
	if (argc == 6)
	{
		ft_values = loadForceTorqueFile(argv[2]);
		input.mu_protein = std::atof(argv[3]);
		input.max_mode = std::atoi(argv[4]);
		input.n_threads = std::atoi(argv[5]);
	}
	else if (argc == 7)
	{
		ft_values.push_back({ std::atof(argv[2]), std::atof(argv[3]) });
		input.mu_protein = std::atof(argv[4]);
		input.max_mode = std::atoi(argv[5]);
		input.n_threads = std::atoi(argv[6]);
	}
	else
	{
		std::cerr << "The program requires one of two input styles:\n";
		std::cerr << "1. DNA length, force, torque, protein mu, number of modes and number of threads (6 inputs)\n";
		std::cerr << "2. DNA length, name of file containing force and torque values, " <<
			"protein mu, number of modes and number of threads (5 inputs)\n";
		exit(2);
	}

	input.DNA_length = std::atof(argv[1]);

	// Calculating parametres from given input
	input.force = ft_values[0][0];
	input.torque = ft_values[0][1];
	Params p = initialiseParams(input);

	vector<double> w3j_all = all_Wigner_3j_protein_torque(input.max_mode - 1);
	//std::cout << "All w3j symbols found\n";

	vector2d L_0 = find_indexed_L_k_r_v2(2 * (input.max_mode - 1), input.max_mode - 1, 0, p.cutoff);
	vector2d L_a_B = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_B, p.cutoff);
	//std::cout << "All L_0 and L_a_B functions found\n";

	vector2d L_a_L = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_L, p.cutoff);
	//std::cout << "All L_a_L functions found\n";

	vector2d L_a_P = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_P, p.cutoff);
	//std::cout << "All L_a_P functions found\n\n";

	vector2d L_a_protein = find_indexed_L_k_r_v2(input.max_mode - 1, input.max_mode - 1, -p.a_protein, p.cutoff);
	for (auto &line : L_a_protein)
		for (auto &value : line)
			value *= std::exp(-p.a_protein);

	vectorcd D_n_ml_A_in = find_all_D_n_ml(input.max_mode, p.A_Euler_angles_in[0], p.A_Euler_angles_in[1], 
		p.A_Euler_angles_in[2]);
	vectorcd D_n_ml_A_out = find_all_D_n_ml(input.max_mode, p.A_Euler_angles_out[0], p.A_Euler_angles_out[1],
		p.A_Euler_angles_out[2]);

	MatrixXd U = create_first_boundary_matrix_v2(input.max_mode, p.L + 3);
	MatrixXd V = create_matrix_V_v2_nucleosome(input.max_mode);

	// Output file
	std::ofstream Output;
	if (argc == 6)
	{
		Output.open("output.csv");
		Output << "# NUS Yan Jie Group @ 2018\n";
		Output << "# force, torque, extension, relative_ext, L_occupancy, P_occupancy, pr_occupancy, linking, " <<
			"superhelical\n";
	}

	int count = 0;
	bool has_negative = false;
	while (count < ft_values.size())
	{
		vector2d vvdMetaResults;
		vector<thread> vtThreads;
		for (int core = 0; core < input.n_threads; ++core)
		{
			vvdMetaResults.emplace_back();
			thread temp(calculate, count, std::ref(ft_values), std::ref(vvdMetaResults), core, std::ref(input),
				p, std::ref(w3j_all), std::ref(L_0), std::ref(L_a_B), std::ref(L_a_L), std::ref(L_a_P),
				std::ref(L_a_protein), std::ref(D_n_ml_A_in), std::ref(D_n_ml_A_out), std::ref(U), std::ref(V));
			vtThreads.push_back(thread(std::move(temp)));
			++count;
			if (count == ft_values.size())
				break;
		}
		for (auto &thread : vtThreads)
			thread.join();
		for (auto &values : vvdMetaResults)
		{
			if (Output)
			{
				for (int nPos = 0; nPos < values.size(); ++nPos)
				{
					Output << values[nPos];
					// Extension
					if (nPos == 2 && values[nPos] < 0.0)
						has_negative = true;
					if (nPos < values.size() - 1)
						Output << ",";
				}
				Output << "\n";
			}
		}
	}

	if (Output)
	{
		if (has_negative)
			Output << "# The file contains negative extension values\n" <<
			"# These result from the DNA being in supercoiled state, which our formula doesn't cover\n" <<
			"# Effective extension for these cases can be considered zero\n";
		Output.close();
	}
	double runtime = global.elapsed();
	std::cout << "Total elapsed time is " << runtime << " seconds.\n";
	if (ft_values.size() > 1)
		std::cout << "Average time for one calculation is " << runtime / ft_values.size() << " seconds.\n";

	return 0;
}