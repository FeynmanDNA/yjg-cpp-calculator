#!/usr/bin/python

import multiprocessing as mp
import subprocess as sp
import numpy as np
import os.path

def transfer_matrix(d, get_array=False):
    if not isinstance(d['force'], list):
        d['force'] = [d['force']]
    if not isinstance(d['torque'], list):
        d['torque'] = [d['torque']]
    f = open('input_ft.dat', 'w')
    for i in d['force']:
        for j in d['torque']:
            f.write('%s %s\n' % (i, j))
    f.close()
    n_cpu = mp.cpu_count()
    print("num of CPU on your PC is: ", n_cpu)
    to_execute = "./WithNul_2018Jul02.out %s %s %s %s %s" % (d['DNALength'], 'input_ft.dat', d['mu_protein'], d['maxmode'], n_cpu)
    p = sp.Popen(to_execute, shell=True)
    p.communicate()

if __name__ == "__main__":
    d = {'force': [0.5+i for i in range(0, 30, 5)],
         'torque': 15.0,
         'mu_protein': 40,
         'maxmode': 14,
         'DNALength': 3400,
         }
    transfer_matrix(d)