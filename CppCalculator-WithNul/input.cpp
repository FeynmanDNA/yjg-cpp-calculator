#include "input.h"

double pi = boost::math::constants::pi<double>();
 
void recalculateForceTorque(Params &p, double force, double torque, double mu_protein, int max_mode)
{
	p.tau = torque / 4.1;
	p.f = force / 4.1;
	p.f_plus = p.plus_coeff * p.f;
	p.f_minus = p.minus_coeff * p.f;

	p.energy_coeff = std::exp((mu_protein + p.protein_mu_correction + 2 * pi * p.tau * p.Lk_protein) *
		p.block_size / p.protein_size);
	p.M = std::pow(max_mode, 2);

	p.chi_B = p.tau * p.lambda_B / 2 / pi / p.c_B;
	p.chi_L = p.tau * p.lambda_L / 2 / pi / p.c_L;
	p.chi_P = p.tau * p.lambda_P / 2 / pi / p.c_P;
	p.omega_B = atan(p.chi_B);
	p.omega_L = atan(p.chi_L);
	p.omega_P = atan(p.chi_P);
	p.B_coeff = (1 - p.lambda_B / 2 / pi) * p.tau;
	p.L_coeff = (1 - p.lambda_L / 2 / pi) * p.tau;
	p.P_coeff = (1 - p.lambda_P / 2 / pi) * p.tau;

	p.spherical_besseli_coeff_r_pr_f = find_all_spherical_Besseli_r(p.r_protein * p.f, 2 * max_mode - 2);
	p.spherical_besseli_coeff_r_pr_f_plus = find_all_spherical_Besseli_r(p.r_protein * p.f_plus, 2 * max_mode - 2);
	p.spherical_besseli_coeff_r_pr_f_minus = find_all_spherical_Besseli_r(p.r_protein * p.f_minus, 2 * max_mode - 2);

	if (std::abs(torque) > 0 && std::abs(p.B_coeff) > 0)
	{
		p.B_coeff_plus = p.plus_coeff * p.B_coeff;
		p.B_coeff_minus = p.minus_coeff * p.B_coeff;
	}
	else
	{
		p.B_coeff_plus = p.plus_coeff - 1;
		p.B_coeff_minus = p.minus_coeff - 1;
	}
	if (std::abs(torque) > 0 && std::abs(p.L_coeff) > 0)
	{
		p.L_coeff_plus = p.plus_coeff * p.L_coeff;
		p.L_coeff_minus = p.minus_coeff * p.L_coeff;
	}
	else
	{
		p.L_coeff_plus = p.plus_coeff - 1;
		p.L_coeff_minus = p.minus_coeff - 1;
	}
	if (std::abs(torque) > 0 && std::abs(p.P_coeff) > 0)
	{
		p.P_coeff_plus = p.plus_coeff * p.P_coeff;
		p.P_coeff_minus = p.minus_coeff * p.P_coeff;
	}
	else
	{
		p.P_coeff_plus = p.plus_coeff - 1;
		p.P_coeff_minus = p.minus_coeff - 1;
	}

	p.besseli_coeff_B = find_all_Besseli_r(p.B_coeff, max_mode - 1);
	p.besseli_coeff_plus_B = find_all_Besseli_r(p.B_coeff_plus, max_mode - 1);
	p.besseli_coeff_minus_B = find_all_Besseli_r(p.B_coeff_minus, max_mode - 1);
	p.besseli_coeff_L = find_all_Besseli_r(p.L_coeff, max_mode - 1);
	p.besseli_coeff_plus_L = find_all_Besseli_r(p.L_coeff_plus, max_mode - 1);
	p.besseli_coeff_minus_L = find_all_Besseli_r(p.L_coeff_minus, max_mode - 1);
	p.besseli_coeff_P = find_all_Besseli_r(p.P_coeff, max_mode - 1);
	p.besseli_coeff_plus_P = find_all_Besseli_r(p.P_coeff_plus, max_mode - 1);
	p.besseli_coeff_minus_P = find_all_Besseli_r(p.P_coeff_minus, max_mode - 1);

	p.coeff_1i_all_B = valarray< complex<double> >(std::pow(pi, 2) * std::exp(-p.a_B - p.c_B), 2 * max_mode - 1);
	p.coeff_1i_all_L = valarray< complex<double> >(std::pow(pi, 2) * std::exp(-p.a_L - p.c_L) *
		std::exp(-p.q * (p.mu_L - 2 * pi * p.tau * p.lk_L_0)), 2 * max_mode - 1);
	p.coeff_1i_all_P = valarray< complex<double> >(std::pow(pi, 2) * std::exp(-p.a_P - p.c_P) *
		std::exp(-p.q * (p.mu_P - 2 * pi * p.tau * p.lk_P_0)), 2 * max_mode - 1);

	// It's double because of issues with multiplying complex and int
	for (double j = -(max_mode - 1); j <= (max_mode - 1); ++j)
	{
		complex<double> coeff_1i = std::pow(1i, -j);
		p.coeff_1i_all_B[j + max_mode - 1] *= coeff_1i;
		p.coeff_1i_all_L[j + max_mode - 1] *= coeff_1i;
		p.coeff_1i_all_P[j + max_mode - 1] *= coeff_1i;
	}

	p.besseli_c_B = find_all_Besseli_r(p.c_B * std::sqrt(1 + std::pow(p.chi_B, 2)), max_mode - 1);
	p.besseli_c_L = find_all_Besseli_r(p.c_L * std::sqrt(1 + std::pow(p.chi_L, 2)), max_mode - 1);
	p.besseli_c_P = find_all_Besseli_r(p.c_P * std::sqrt(1 + std::pow(p.chi_P, 2)), max_mode - 1);

	p.coeff_omega_B = valarray< complex<double> >(1.0, 2 * max_mode - 1);
	p.coeff_omega_L = valarray< complex<double> >(1.0, 2 * max_mode - 1);
	p.coeff_omega_P = valarray< complex<double> >(1.0, 2 * max_mode - 1);

	// It's double because of issues with multiplying complex and int
	for (double j = -(max_mode - 1); j <= (max_mode - 1); ++j)
	{
		p.coeff_omega_B[j + max_mode - 1] = std::exp(-1i * j * p.omega_B);
		p.coeff_omega_L[j + max_mode - 1] = std::exp(-1i * j * p.omega_L);
		p.coeff_omega_P[j + max_mode - 1] = std::exp(-1i * j * p.omega_P);
	}
}

bool isEmpty(string strInput)
{
	int nCount = 0;

	while (strInput[nCount])
	{
		if (!std::isspace(strInput[nCount])) return false;
		++nCount;
	}

	return true;
}

string trimWhitespace(string strInput)
{
	size_t nFirst = strInput.find_first_not_of(" \t");
	size_t nSecond = strInput.find_last_not_of(" \t");

	string strOutput = strInput.substr(nFirst, nSecond - nFirst + 1);

	return strOutput;
}

vector<double> separateStringDoubles(string strSequence)
{
	vector<double> vdSequence;
	string strMember;
	int nCount = 0;

	while (strSequence[nCount])
	{
		if (!strncmp(&strSequence[nCount], ",", 1))
		{
			double dMember = std::stod(strMember);
			vdSequence.push_back(dMember);
			strMember.clear();
		}
		else
			strMember += strSequence[nCount];

		++nCount;
	}

	double dMember = std::stod(strMember);
	vdSequence.push_back(dMember);
	strMember.clear();

	return vdSequence;
}

Params initialiseParams(Input &input)
{
	Params p;

	std::ifstream InputStream("input.dat");

	if (!InputStream)
		std::cout << "No input file (input.dat) found, using default values\n";

	// Update default values (created at the time of p creation) with input
	while (InputStream)
	{
		string strLine;
		getline(InputStream, strLine);

		// Ignore comment or empty lines
		if (!std::strncmp(strLine.c_str(), "#", 1) || isEmpty(strLine))
			continue;

		// Remove everything after #
		size_t nPos = strLine.find_first_of('#');
		if (nPos != string::npos)
			strLine.erase(nPos);

		assignInput(strLine, p);
	}

	p.a_B = p.A_B / p.b_B;
	p.c_B = p.C_B / p.b_B;
	p.q = 10.4 * p.b_B / 3.4;
	p.a_L = p.A_L / p.b_L;
	p.c_L = p.C_L / p.b_L;
	p.a_P = p.A_P / p.b_P;
	p.c_P = p.C_P / p.b_P;
	p.mu_L_plus = p.plus_coeff * p.mu_L;
	p.mu_L_minus = p.minus_coeff * p.mu_L;
	p.mu_P_plus = p.plus_coeff * p.mu_P;
	p.mu_P_minus = p.minus_coeff * p.mu_P;

	p.N = std::floor(input.DNA_length / p.b_B) - 1;
	// Change DNA length to conform to block_size if necessary
	if (std::fmod(p.N, p.block_size) != 0)
	{
		p.N = (p.N / p.block_size + 1) * p.block_size;
		input.DNA_length = (p.N + 1) * p.b_B;

		std::cout << "The DNA length has been adjusted to " << input.DNA_length << "nm\n\n";
	}
	p.power_num = p.N / p.block_size;  // Intentionally int

	p.r_protein = 6.4 / p.protein_size;
    p.L = p.protein_size / p.block_size;  // Intentionally int

	if (std::abs(input.mu_protein) > 0.1)
	{
		p.mu_protein_plus = input.mu_protein + (p.plus_coeff - 1) * std::abs(input.mu_protein);
		p.mu_protein_minus = input.mu_protein + (p.minus_coeff - 1) * std::abs(input.mu_protein);
	}
	else
	{
		p.mu_protein_plus = 0.0001;
		p.mu_protein_minus = -0.0001;
	}

	p.coeff_1i_all = valarray< complex<double> >(1.0, 2 * input.max_mode - 1);
	for (double j = -(input.max_mode - 1); j <= (input.max_mode - 1); ++j)
		p.coeff_1i_all[j + input.max_mode - 1] = std::pow(1i, -j);

	p.coeff_1i_all_T30 = valarray<double>(4 * std::pow(pi, 2), 2 * input.max_mode - 1);
	p.coeff_1i_all_T12 = valarray<double>(1.0, 2 * input.max_mode - 1);
	for (int j = -(input.max_mode - 1); j <= input.max_mode - 1; ++j)
	{
		int sign = std::pow(-1, j);
		p.coeff_1i_all_T30[j + input.max_mode - 1] *= sign;
		p.coeff_1i_all_T12[j + input.max_mode - 1] = sign;
	}

	p.besseli_coeff_c_protein = std::exp(-p.c_protein / 2) * find_all_Besseli_r(p.c_protein, input.max_mode - 1) *
		std::exp(-p.c_protein / 2);

	recalculateForceTorque(p, input.force, input.torque, input.mu_protein, input.max_mode);

	return p;
}

vector2d loadForceTorqueFile(string filename)
{
	vector2d ftData;

	std::ifstream InputStream(filename.c_str());

	if (!InputStream)
	{
		std::cerr << "Couldn't open file " << filename << std::endl;
		exit(3);
	}

	while (InputStream)
	{
		string strLine;
		getline(InputStream, strLine);
		// Ignore comment or empty lines
		// Remove comments
		for (unsigned int nChar = 0; nChar < strLine.size(); ++nChar)
		{
			if (strLine[nChar] == '#')
			{
				strLine = strLine.substr(0, nChar);
				break;
			}
		}
		// Ignore empty lines
		if (std::all_of(strLine.begin(), strLine.end(), isspace))
			continue;

		std::stringstream StringStream;
		StringStream << strLine;

		vector<double> vdDataLine;
		while (!StringStream.eof())
		{
			string strValue;
			StringStream >> strValue;

			if (!strValue.empty())
			{
				double dValue = std::atof(strValue.c_str());
				vdDataLine.push_back(dValue);
			}
		}
		ftData.push_back(vdDataLine);
	}

	return ftData;
}

void assignInput(string strLine, Params &p)
{
	string strFirstInput, strSecondInput;

	size_t nPos = strLine.find_first_of('=');
	if (nPos == string::npos)
	{
		std::cerr << "The following line doesn't contain any assignment:" << std::endl;
		std::cerr << strLine << std::endl;
		exit(4);
	}
	else
	{
		strFirstInput = trimWhitespace(strLine.substr(0, nPos));
		strSecondInput = trimWhitespace(strLine.substr(nPos + 1));
	}

	if (!strFirstInput.compare("b_B")) p.b_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_B")) p.A_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_B")) p.C_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_B")) p.lambda_B = std::stof(strSecondInput);
	if (!strFirstInput.compare("b_L")) p.b_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_L")) p.A_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_L")) p.C_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_L")) p.lambda_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_L")) p.mu_L = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_L_0")) p.lk_L_0 = std::stof(strSecondInput);
	if (!strFirstInput.compare("b_P")) p.b_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_P")) p.A_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("C_P")) p.C_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("lambda_P")) p.lambda_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("mu_P")) p.mu_P = std::stof(strSecondInput);
	if (!strFirstInput.compare("lk_P_0")) p.lk_P_0 = std::stof(strSecondInput);
	if (!strFirstInput.compare("a_protein")) p.a_protein = std::stof(strSecondInput);
	if (!strFirstInput.compare("c_protein")) p.c_protein = std::stof(strSecondInput);
	if (!strFirstInput.compare("protein_mu_correction")) p.protein_mu_correction = std::stof(strSecondInput);
	if (!strFirstInput.compare("A_Euler_angles_in")) p.A_Euler_angles_in = separateStringDoubles(strSecondInput);
	if (!strFirstInput.compare("A_Euler_angles_out")) p.A_Euler_angles_out = separateStringDoubles(strSecondInput);
	if (!strFirstInput.compare("protein_size")) p.protein_size = std::stof(strSecondInput);
	if (!strFirstInput.compare("Lk_protein")) p.Lk_protein = std::stof(strSecondInput);
	if (!strFirstInput.compare("block_size")) p.block_size = std::stoi(strSecondInput);
	if (!strFirstInput.compare("coop")) p.coop = std::stof(strSecondInput);
	if (!strFirstInput.compare("cutoff")) p.cutoff = std::stof(strSecondInput);
	if (!strFirstInput.compare("plus_coeff")) p.plus_coeff = std::stof(strSecondInput);
	if (!strFirstInput.compare("minus_coeff")) p.minus_coeff = std::stof(strSecondInput);
}