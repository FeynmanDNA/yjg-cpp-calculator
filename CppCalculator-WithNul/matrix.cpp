#include "matrix.h"

MatrixXd create_first_boundary_matrix_v2(int M, int N_tot)
{
	// function creates the first boundary condition matrix U

	MatrixXd U = MatrixXd::Zero(M, N_tot * M);

	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < 3 * M; ++j)
		{
			if (j % M == i)
				U(i, j) = 1.0;
		}
	}

	return U;
}

MatrixXd create_matrix_V_v2_nucleosome(int max_mode)
{
	MatrixXd V = MatrixXd::Zero(max_mode, max_mode);

	for (int p = 0; p < max_mode; ++p)
		for (int q = 0; q < max_mode; ++q)
			V(p, q) = std::sqrt((2 * p + 1) * (2 * q + 1)) / 4 / pi;

	return V;
}

vector<MatrixXcd> create_matrix_block_SB_SL_SP_Sprotein_v10_nucleosome(Params &ps, int max_mode, vector<double> &w3j_all,
	vector2d &L_0, vector2d &L_a_B, vector2d &L_b_B_f, vector2d &L_b_B_f_plus, vector2d &L_b_B_f_minus, vector2d &L_a_L, 
	vector2d &L_b_L_f, vector2d &L_b_L_f_plus, vector2d &L_b_L_f_minus, vector2d &L_a_P, vector2d &L_b_P_f, 
	vector2d &L_b_P_f_plus, vector2d &L_b_P_f_minus, vector2d &L_a_protein, vectorcd &D_n_ml_A_in, vectorcd &D_n_ml_A_out)
{
	MatrixXcd m_S_B = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_L = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_P = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_protein_A_in = MatrixXcd::Zero(ps.M, ps.M);
	MatrixXcd m_S_protein_A_out = MatrixXcd::Zero(ps.M, ps.M);
	MatrixXcd m_S_protein_B = MatrixXcd::Zero(ps.M, ps.M);

	MatrixXcd m_S_B_plus_f = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_L_plus_f = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_P_plus_f = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_B_plus_t = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_L_plus_t = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_P_plus_t = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_protein_plus_A_in_f = MatrixXcd::Zero(ps.M, ps.M);
	MatrixXcd m_S_protein_plus_A_out_f = MatrixXcd::Zero(ps.M, ps.M);
	MatrixXcd m_S_protein_plus_B_f = MatrixXcd::Zero(ps.M, ps.M);

	MatrixXcd m_S_B_minus_f = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_L_minus_f = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_P_minus_f = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_B_minus_t = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_L_minus_t = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_P_minus_t = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd m_S_protein_minus_A_in_f = MatrixXcd::Zero(ps.M, ps.M);
	MatrixXcd m_S_protein_minus_A_out_f = MatrixXcd::Zero(ps.M, ps.M);
	MatrixXcd m_S_protein_minus_B_f = MatrixXcd::Zero(ps.M, ps.M);

	for (int n1 = 0; n1 < max_mode; ++n1)
	{
		int index_1 = n1 * (n1 + 1);

		for (int n2 = 0; n2 < max_mode; ++n2)
		{
			double prefactor_1 = std::sqrt((2 * n1 + 1) * (2 * n2 + 1));
			int index_2 = n2 * (n2 + 1);

			/***************** Bare DNA *****************/

			for (int p = 0; p < max_mode; ++p)
			{
				double prefactor_2 = prefactor_1 * (2 * p + 1);

				for (int k1 = std::abs(n1 - p); k1 <= n1 + p; ++k1)
				{
					double prefactor_3 = prefactor_2 * (2 * k1 + 1);

					for (int k2 = std::abs(n2 - p); k2 <= n2 + p; ++k2)
					{
						double prefactor_4 = prefactor_3 * (2 * k2 + 1);

						for (int r = -std::min({ p, k1, k2 }); r <= std::min({ p, k1, k2 }); ++r)
						{
							int ind_r = std::abs(r);
							int ind_pr = max_mode + r - 1;
							int ind_mr = max_mode - r - 1;

							/***************** B - DNA *****************/

							double w_3j_sym = prefactor_4 * L_0[k2][ind_pr] * std::pow(w3j_all[index(n1, p, k1, 
								max_mode - 1, ind_mr, max_mode - 1)] * w3j_all[index(n2, p, k2, max_mode - 1, ind_mr,
								max_mode - 1)], 2);
							complex<double> matr_elem_general = ps.coeff_1i_all_B[ind_pr] * ps.coeff_omega_B[ind_mr] * 
								ps.besseli_c_B[ind_r] * L_a_B[p][ind_pr] * w_3j_sym;

							complex<double> matr_elem = matr_elem_general * ps.besseli_coeff_B[ind_r];
							m_S_B(n1, n2) += matr_elem * L_b_B_f[k1][ind_pr];
							m_S_B_plus_f(n1, n2) += matr_elem * L_b_B_f_plus[k1][ind_pr];
							m_S_B_minus_f(n1, n2) += matr_elem * L_b_B_f_minus[k1][ind_pr];

							matr_elem = matr_elem_general * ps.besseli_coeff_plus_B[ind_r];
							m_S_B_plus_t(n1, n2) += matr_elem * L_b_B_f[k1][ind_pr];
							matr_elem = matr_elem_general * ps.besseli_coeff_minus_B[ind_r];
							m_S_B_minus_t(n1, n2) += matr_elem * L_b_B_f[k1][ind_pr];

							/***************** L - DNA *****************/

							matr_elem_general = ps.coeff_1i_all_L[ind_pr] * ps.coeff_omega_L[ind_mr] * 
								ps.besseli_c_L[ind_r] * L_a_L[p][ind_pr] * w_3j_sym;

							matr_elem = matr_elem_general * ps.besseli_coeff_L[ind_r];
							m_S_L(n1, n2) += matr_elem * L_b_L_f[k1][ind_pr];
							m_S_L_plus_f(n1, n2) += matr_elem * L_b_L_f_plus[k1][ind_pr];
							m_S_L_minus_f(n1, n2) += matr_elem * L_b_L_f_minus[k1][ind_pr];

							matr_elem = matr_elem_general * ps.besseli_coeff_plus_L[ind_r];
							m_S_L_plus_t(n1, n2) += matr_elem * L_b_L_f[k1][ind_pr];
							matr_elem = matr_elem_general * ps.besseli_coeff_minus_L[ind_r];
							m_S_L_minus_t(n1, n2) += matr_elem * L_b_L_f[k1][ind_pr];

							/***************** P - DNA *****************/

							matr_elem_general = ps.coeff_1i_all_P[ind_pr] * ps.coeff_omega_P[ind_mr] * 
								ps.besseli_c_P[ind_r] * L_a_P[p][ind_pr] * w_3j_sym;

							matr_elem = matr_elem_general * ps.besseli_coeff_P[ind_r];
							m_S_P(n1, n2) += matr_elem * L_b_P_f[k1][ind_pr];
							m_S_P_plus_f(n1, n2) += matr_elem * L_b_P_f_plus[k1][ind_pr];
							m_S_P_minus_f(n1, n2) += matr_elem * L_b_P_f_minus[k1][ind_pr];

							matr_elem = matr_elem_general * ps.besseli_coeff_plus_P[ind_r];
							m_S_P_plus_t(n1, n2) += matr_elem * L_b_P_f[k1][ind_pr];
							matr_elem = matr_elem_general * ps.besseli_coeff_minus_P[ind_r];
							m_S_P_minus_t(n1, n2) += matr_elem * L_b_P_f[k1][ind_pr];
						}
					}
				}
			}

			/***************** T_01 factor *****************/
			
			double multiplier(0.0), multiplier_plus(0.0), multiplier_minus(0.0);

			for (int k = std::abs(n1 - n2); k <= n1 + n2; ++k)
			{
				double matr_elem = (2 * k + 1) * std::pow(w3j_all[index(n1, n2, k, max_mode - 1, max_mode - 1, 
					max_mode - 1)], 2);
				
				multiplier += matr_elem * L_b_B_f[k][max_mode - 1];
				multiplier_plus += matr_elem * L_b_B_f_plus[k][max_mode - 1];
				multiplier_minus += matr_elem * L_b_B_f_minus[k][max_mode - 1];
			}

			for (int t = -n2; t <= n2; ++t)
			{
				int index_2b = index_2 + t;
				int ind_t = max_mode + t - 1;
				int abs_t = std::abs(t);
				complex<double> prefactor_2 = 4 * std::pow(pi, 3) * prefactor_1 * ps.besseli_coeff_c_protein[abs_t] * 
					L_a_protein[n2][ind_t] * D_n_ml_A_in[index(n2, max_mode - 1, ind_t, max_mode)];

				m_S_protein_A_in(index_1, index_2b) += multiplier * prefactor_2;
				m_S_protein_plus_A_in_f(index_1, index_2b) += multiplier_plus * prefactor_2;
				m_S_protein_minus_A_in_f(index_1, index_2b) += multiplier_minus * prefactor_2;
			}

			/***************** T_12 = T_23 *****************/

			for (int t = -std::min(n1, n2); t <= std::min(n1, n2); ++t)
			{
				int index_1b = index_1 + t;
				int index_2b = index_2 + t;
				int ind_t = max_mode + t - 1;
				int ind_mt = max_mode - t - 1;
				complex<double> prefactor_2 = prefactor_1 * ps.coeff_1i_all_T12[ind_t];

				for (int k = std::abs(n1 - n2); k <= n1 + n2; ++k)
				{
					complex<double> matr_elem = prefactor_2 * w3j_all[index(n1, n2, k, max_mode - 1, max_mode - 1,
						max_mode - 1)] * w3j_all[index(n1, n2, k, ind_mt, ind_t, max_mode - 1)] * (2.0 * k + 1);

					m_S_protein_B(index_1b, index_2b) += matr_elem * ps.spherical_besseli_coeff_r_pr_f[k];
					m_S_protein_plus_B_f(index_1b, index_2b) += matr_elem * ps.spherical_besseli_coeff_r_pr_f_plus[k];
					m_S_protein_minus_B_f(index_1b, index_2b) += matr_elem * ps.spherical_besseli_coeff_r_pr_f_minus[k];
				}
			}

			/***************** T_30 *****************/

			for (int t = -std::min(n1, n2); t <= std::min(n1, n2); ++t)
			{
				int index_1b = index_1 + t;
				int ind_t = max_mode + t - 1;
				int ind_mt = max_mode - t - 1;
				complex<double> prefactor_2 = prefactor_1 * ps.coeff_1i_all_T30[ind_t] * ps.besseli_coeff_c_protein[0] * 
					L_a_protein[n2][max_mode - 1] * D_n_ml_A_out[index(n2, ind_t, max_mode - 1, max_mode)];

				for (int k = std::abs(n1 - n2); k <= n1 + n2; ++k)
				{
					complex<double> matr_elem = prefactor_2 * w3j_all[index(n1, n2, k, max_mode - 1, max_mode - 1,
						max_mode - 1)] * w3j_all[index(n1, n2, k, ind_mt, ind_t, max_mode - 1)] * (2.0 * k + 1);

					m_S_protein_A_out(index_1b, index_2) += matr_elem * ps.spherical_besseli_coeff_r_pr_f[k];
					m_S_protein_plus_A_out_f(index_1b, index_2) += matr_elem * ps.spherical_besseli_coeff_r_pr_f_plus[k];
					m_S_protein_minus_A_out_f(index_1b, index_2) += matr_elem * ps.spherical_besseli_coeff_r_pr_f_minus[k];
				}
			}
		}
	}

	return vector<MatrixXcd> {m_S_B, m_S_L, m_S_P, m_S_protein_A_in, m_S_protein_A_out, m_S_protein_B, m_S_B_plus_f, 
		m_S_L_plus_f, m_S_P_plus_f, m_S_protein_plus_A_in_f, m_S_protein_plus_A_out_f, m_S_protein_plus_B_f, 
		m_S_B_minus_f, m_S_L_minus_f, m_S_P_minus_f, m_S_protein_minus_A_in_f, m_S_protein_minus_A_out_f, 
		m_S_protein_minus_B_f, m_S_B_plus_t, m_S_L_plus_t, m_S_P_plus_t, m_S_B_minus_t, m_S_L_minus_t, m_S_P_minus_t};
}

MatrixXcd create_protein_matrix_v1_nucleosome(const MatrixXcd &m_S_protein_A_in, const MatrixXcd &m_S_protein_plus_A_out,
	const MatrixXcd &m_S_protein_B, double protein_size, int max_mode)
{
	// Function calculates the last protein block in the DNA transfer - matrix

	MatrixXcd S_protein = MatrixXcd::Zero(max_mode, max_mode);
	MatrixXcd S_protein_temp = m_S_protein_A_in * matr_pow(m_S_protein_B, protein_size - 1) * m_S_protein_plus_A_out;

	for (int i = 0; i < max_mode; ++i)
		for (int j = 0; j < max_mode; ++j)
			S_protein(i, j) = S_protein_temp(i * (i + 1), j * (j + 1));

	return S_protein;
}

MatrixXcd create_transfer_matrix_nucleosome_v7(Params &p, const MatrixXcd &m_S_B, const MatrixXcd &m_S_L, 
	const MatrixXcd &m_S_P, const MatrixXcd &m_S_protein, int M, double energy_coeff)
{
	// This function creates the main transfer matrix

	MatrixXcd S_protein_mu = m_S_protein * energy_coeff;
	MatrixXcd I = MatrixXcd::Identity(M, M) * energy_coeff;
	
	MatrixXcd S_B_power = matr_pow(m_S_B, p.block_size);
	MatrixXcd S_L_power = matr_pow(m_S_L, p.block_size);
	MatrixXcd S_P_power = matr_pow(m_S_P, p.block_size);

	int size = (p.L + 3) * M;
	MatrixXcd S = MatrixXcd::Zero(size, size);

	S.block(0, 0, M, M) = S_B_power;
	S.block(0, M, M, M) = S_B_power * std::exp(-p.coop);
	S.block(0, 2 * M, M, M) = S_B_power * std::exp(-p.coop);

	S.block(M, 0, M, M) = S_L_power * std::exp(-p.coop);
	S.block(M, M, M, M) = S_L_power;
	S.block(M, 2 * M, M, M) = S_L_power * std::exp(-p.coop);

	S.block(2 * M, 0, M, M) = S_P_power * std::exp(-p.coop);
	S.block(2 * M, M, M, M) = S_P_power * std::exp(-p.coop);
	S.block(2 * M, 2 * M, M, M) = S_P_power;

	/***************** Protein part *****************/

	S.block(0, 3 * M, M, M) = matr_pow(m_S_B, p.block_size - 1);
	S.block(M, 3 * M, M, M) = matr_pow(m_S_L, p.block_size - 1) * std::exp(-p.coop);
	S.block(2 * M, 3 * M, M, M) = matr_pow(m_S_P, p.block_size - 1) * std::exp(-p.coop);

	S.block((p.L + 2) * M, 0, M, M) = S_protein_mu;
	S.block((p.L + 2) * M, M, M, M) = S_protein_mu * std::exp(-p.coop);
	S.block((p.L + 2) * M, 2 * M, M, M) = S_protein_mu * std::exp(-p.coop);

	for (int ind = 3; ind <= p.L + 1; ++ind)
		S.block(ind * M, (ind + 1) * M, M, M) = I;

	return S;
}

MatrixXd create_first_boundary_matrix_torque_v3(Params &p, double mu_L, double mu_P, double f, int M, int N_tot, 
	MatrixXd &V)
{
	MatrixXd O = MatrixXd::Zero(N_tot * M, M);

	O.block(0, 0, M, M) = std::exp(p.b_B * f) * V;
	O.block(M, 0, M, M) = std::exp(p.b_L * f) * std::exp(-p.q * (mu_L - 2 * pi * p.tau * p.lk_L_0)) * V;
	O.block(2 * M, 0, M, M) = std::exp(p.b_P * f) * std::exp(-p.q * (mu_P - 2 * pi * p.tau * p.lk_P_0)) * V;

	return O;
}

//MatrixScd createIdentity(int size)
//{
//	vector<Triplet> tripletList;
//	tripletList.reserve(size);
//
//	for (int i = 0; i < size; ++i)
//		tripletList.push_back(Triplet(i, i, 1.0));
//
//	MatrixScd mat(size, size);
//	mat.setFromTriplets(tripletList.begin(), tripletList.end());
//	
//	return mat;
//}