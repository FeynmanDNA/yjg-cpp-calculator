#pragma once

#ifndef MATRIX_H_
#define MATRIX_H_

#include <vector>
#include <cmath>
#include <bitset>
#include <string>
#include <Eigen/Core>
// #include <Eigen/SparseCore>

// #include "eigen.h"
#include "input.h"

using std::vector;
using std::string;
using Eigen::Matrix;
using Eigen::MatrixXd;
using Eigen::MatrixXcd;
//using Eigen::SparseMatrix;
using Eigen::Dynamic;

//using Triplet = Eigen::Triplet < complex<double> >;
//using MatrixScd = Eigen::SparseMatrix< complex<double> >;
using vector2d = vector< vector<double> >;

// This enum is meant to facilitate choosing the right matrix from the vector
enum MatrixS
{
	S_B, S_L, S_P, S_protein_A_in, S_protein_A_out, S_protein_B, S_B_plus_f, S_L_plus_f, S_P_plus_f,
	S_protein_plus_A_in_f, S_protein_plus_A_out_f, S_protein_plus_B_f, S_B_minus_f, S_L_minus_f, S_P_minus_f, 
	S_protein_minus_A_in_f, S_protein_minus_A_out_f, S_protein_minus_B_f, S_B_plus_t, S_L_plus_t, S_P_plus_t, 
	S_B_minus_t, S_L_minus_t, S_P_minus_t
};

MatrixXd create_first_boundary_matrix_v2(int M, int N_tot);
MatrixXd create_matrix_V_v2_nucleosome(int max_mode);
vector<MatrixXcd> create_matrix_block_SB_SL_SP_Sprotein_v10_nucleosome(Params &ps, int max_mode, vector<double> &w3j_all,
	vector2d &L_0, vector2d &L_a_B, vector2d &L_b_B_f, vector2d &L_b_B_f_plus, vector2d &L_b_B_f_minus, vector2d &L_a_L,
	vector2d &L_b_L_f, vector2d &L_b_L_f_plus, vector2d &L_b_L_f_minus, vector2d &L_a_P, vector2d &L_b_P_f,
	vector2d &L_b_P_f_plus, vector2d &L_b_P_f_minus, vector2d &L_a_protein, vectorcd &D_n_ml_A_in, vectorcd &D_n_ml_A_out);
MatrixXcd create_protein_matrix_v1_nucleosome(const MatrixXcd &m_S_protein_A_in, const MatrixXcd &m_S_protein_plus_A_out,
	const MatrixXcd &m_S_protein_B, double protein_size, int max_mode);
MatrixXcd create_transfer_matrix_nucleosome_v7(Params &p, const MatrixXcd &m_S_B, const MatrixXcd &m_S_L,
	const MatrixXcd &m_S_P, const MatrixXcd &m_S_protein, int M, double energy_coeff);
MatrixXd create_first_boundary_matrix_torque_v3(Params &p, double mu_L, double mu_P, double f, int M, 
	int N_tot, MatrixXd &V);
//MatrixScd createIdentity(int size);

template<typename T>
Matrix<T, Dynamic, Dynamic> matr_pow(const Matrix<T, Dynamic, Dynamic> &S, int N)
{
	Matrix<T, Dynamic, Dynamic> R = S;

	for (int n = 1; n < N; ++n)
		R *= S;

	return R;
}

//template<typename T>
//T getMax(const SparseMatrix<T> &S)
//{
//	T max_val;
//	bool first = true;
//
//	for (int k = 0; k < S.outerSize(); ++k)
//	{
//		for (typename SparseMatrix<T>::InnerIterator it(S, k); it; ++it)
//		{
//			if (first)
//			{
//				first = false;
//				max_val = std::abs(it.value());
//			}
//			else if (std::abs(it.value()) > std::abs(max_val))
//				max_val = std::abs(it.value());
//		}
//	}
//
//	return max_val;
//}

//template<typename T>
//void single_use_product(Matrix<T, Dynamic, Dynamic> &S_plus, Matrix<T, Dynamic, Dynamic> &S_minus, int N, 
//	T &log_coeff_plus_prod, T &log_coeff_minus_prod)
//{
//	if (N == 256)
//	{
//		T coeff_plus = S_plus.cwiseAbs().maxCoeff();
//		T coeff_minus = S_minus.cwiseAbs().maxCoeff();
//
//		log_coeff_plus_prod = std::log(coeff_plus);
//		log_coeff_minus_prod = std::log(coeff_minus);
//
//		S_plus /= coeff_plus;
//		S_minus /= coeff_minus;
//
//		for (int n = 1; n <= 2; ++n)
//		{
//			S_plus = S_plus * S_plus;
//			S_plus = S_plus * S_plus;
//			S_plus = S_plus * S_plus;
//			S_plus = S_plus * S_plus;
//
//			S_minus = S_minus * S_minus;
//			S_minus = S_minus * S_minus;
//			S_minus = S_minus * S_minus;
//			S_minus = S_minus * S_minus;
//
//			coeff_plus = S_plus.cwiseAbs().maxCoeff();
//			coeff_minus = S_minus.cwiseAbs().maxCoeff();
//
//			S_plus /= coeff_plus;
//			S_minus /= coeff_minus;
//
//			log_coeff_plus_prod = 16.0 * log_coeff_plus_prod + log(coeff_plus);
//			log_coeff_minus_prod = 16.0 * log_coeff_minus_prod + log(coeff_minus);
//		}
//	}
//	else
//	{
//		std::cerr << "Wrong N for single use product\n";
//		exit(1);
//	}
//}

//template<typename T>
//Matrix<T, Dynamic, Dynamic> square_blockwise(const Matrix<T, Dynamic, Dynamic> &S, int blocks)
//{
//	int S_size = S.rows();
//	int block_size = S_size / blocks;
//	Matrix<T, Dynamic, Dynamic> A = Matrix<T, Dynamic, Dynamic>::Zero(S_size, S_size);
//
//	for (int i = 0; i < blocks; ++i)
//	{
//		for (int j = 0; j < blocks; ++j)
//		{
//			for (int k = 0; k < blocks; ++k)
//				A.block(i, j, block_size, block_size) += 
//					S.block(i, k, block_size, block_size) * S.block(k, j, block_size, block_size);
//		}
//	}
//
//	return A;
//}

template<typename T>
Matrix<T, Dynamic, Dynamic> quick_matrices_product_v3(const Matrix<T, Dynamic, Dynamic> &S, int N, T &log_coeff)
{
	string bin = std::bitset<32>(N).to_string();
	while (bin[0] == '0')
		bin.erase(0, 1);
	int bin_size = bin.size();

	int S_size = S.rows();
	Matrix<T, Dynamic, Dynamic> A = Matrix<T, Dynamic, Dynamic>::Identity(S_size, S_size);

	log_coeff = 0.0;
	auto coeff_current = S.cwiseAbs().maxCoeff();
	Matrix<T, Dynamic, Dynamic> A_current = S / coeff_current;
	auto log_coeff_current = std::log(coeff_current);

	for (int i = 0; i < bin_size; ++i)
	{
		T coeff_A;

		switch (bin[bin_size - 1 - i])
		{
		case '1':
			A *= A_current;
			coeff_A = A.cwiseAbs().maxCoeff();
			A = A / coeff_A;
			log_coeff += log_coeff_current + std::log(coeff_A);
			break;

		case '0':
			break;

		default:
			std::cerr << bin << " " << bin[bin_size - i] << "\n";
			std::cerr << "Matrices quick product error\n";
			exit(1);
		}

		if (i != bin_size - 1)
		{
			//if (i == 4 || i == 0)
			//	for (int row = 0; row < S_size; ++row)
			//		for (int col = 0; col < S_size; ++col)
			//			if (std::abs(A_current(row, col)) < 1e-21)
			//				A_current(row, col) == 0.0;

			A_current = A_current * A_current;
			auto coeff_current_new = A_current.cwiseAbs().maxCoeff();
			A_current /= coeff_current_new;
			log_coeff_current = 2.0 * log_coeff_current + std::log(coeff_current_new);
		}
	}

	return A;
}

#endif