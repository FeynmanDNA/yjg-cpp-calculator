#pragma once

#ifndef INPUT_H_
#define INPUT_H_

#include <cmath>
#include <valarray>
#include <fstream>
#include <string>
#include <cctype>
#include <vector>
#include <sstream>
#include <algorithm>
#include <complex>
#include <iostream>
//#include <experimental/filesystem>

//#include <boost/filesystem.hpp>
#include <boost/math/constants/constants.hpp>
#include "functions.h"

using std::valarray;
using std::string;
using std::vector;
using std::complex;

using vector2d = vector< vector<double> >;
//namespace fs = std::experimental::filesystem;
//namespace fs = boost::filesystem;

extern double pi;

struct Input
{
	double force = 0.1;        // in[pN] units
	double torque = 0.0;       // in[pN*nm] units
	double DNA_length = 3400;  // DNA length in B - DNA state in[nm] units
	double mu_protein = 40;
	int max_mode = 14;         // number of modes taken into account in the transfer matrix calculations (~10 - 15)
	int n_threads = 1;         // number of concurrent threads to be launched
};

struct Params
{
	// *** Default values ***
	// B-DNA
	double b_B = 0.5;          // size of individual DNA segment, b, in B - DNA state in[nm] units
	double A_B = 50.0;         // B - DNA bending persistence length
	double C_B = 95.0;         // B - DNA twisting persistence length 							   
	double lambda_B = 4.3;     // lambda parameter for B - DNA
	// L-DNA
	double b_L = 1.35 * b_B;      // size of individual DNA segment in L - DNA state
	double A_L = 7.0;             // L - DNA bending persistence length   			  
	double C_L = 15.0;            // L - DNA twisting persistence length					  
	double lambda_L = 4.3;        // lambda parameter for L - DNA
	double mu_L = 4.9;            // the energy difference in k_B*T units at zero torque per single DNA base - pair corresponding to the transition from B - DNA to L - DNA
	double lk_L_0 = -1.0 / 16 - 1 / 10.4;  // the DNA relaxed linking number change per base - pair during transition from B - DNA to L - DNA, 16 bp - helical repeat of L - DNA, 10.5 bp - helical repeat of B - DNA
	double coop = 9.0;            // energy penalty appearing due to B - L - DNA, B - P - DNA and P - L - DNA boundaries
	// P-DNA
	double b_P = 1.7 * b_B;           // size of individual DNA segment in P - DNA state
	double A_P = 15.0;                // P - DNA bending persistence length
	double C_P = 25.0;                // P - DNA twisting persistence length       
	double lambda_P = -0.5;           // lambda parameter for P - DNA
	double mu_P = 17.8;               // the energy difference in k_B*T units at zero torque per single DNA base - pair corresponding to the transition from B - DNA to P - DNA
	double lk_P_0 = 1.0 / 3 - 1 / 10.4;     // the DNA relaxed linking number change per base - pair during transition from B - DNA to P - DNA, 3 bp - helical repeat of P - DNA, 10.5 bp - helical repeat of B - DNA
	// Nucleoprotein
	double a_protein = 200.0 / 6;  // like for DNA - stiffening protein, bending elasticity in[k_B*T] units
	double c_protein = 200.0 / 6;  // like for DNA - stiffening protein, twisting elasticity in[k_B*T] units
	double protein_mu_correction = -431.96;
	//double mu_protein = 40.0;  // based on the Yan Jie's measurements of reconstituted nucleosomal array [Yan et al, Mol. Biol. Cell, 2007, 18:464], the average equilibrium detachment force for nucleosomes ~ 3.5 pN and change of the legnth ~ 50 nm => binding energy ~ 40 kT
	vector<double> A_Euler_angles_in = { 2.1253, 2.1154, -0.7887 };   // equilibrium Euler angles for the protein complex
	vector<double> A_Euler_angles_out = { -0.7888, 2.1154, 2.1049 };  // equilibrium Euler angles for the protein complex
	double protein_size = 96.0;  // histone, 48nm of the wrapped DNA = 96 segments of 0.5nm length
	double Lk_protein = -1.2;
	int block_size = 12;
	// Other
	double cutoff = pi;
	double plus_coeff = 1.0001;
	double minus_coeff = 0.9999;

	// *** Calculated after default adjustments ***
	double a_B;  // B - DNA bending elasticity
	double c_B;  // B - DNA twisting elasticity
	double q;    // number of DNA base - pairs in individual DNA segment
	double a_L;  // L - DNA bending elasticity
	double c_L;  // L - DNA twisting elasticity
	double a_P;  // P - DNA bending elasticity
	double c_P;  // P - DNA twisting elasticity
	double mu_L_plus;
	double mu_L_minus;
	double mu_P_plus;
	double mu_P_minus;
	double r_protein;  // nucleoprotein complex distance between the DNA entry to the exit points divided by the total number of DNA segments in the complex, [nm]
	int L;

	// *** Derived from input ***
	int N;  // total number of DNA segments in the polymer chain
	double tau;                     // torque / k_B / T
	double f;                       // force / k_B / T
	double energy_coeff;
	int power_num;
	int M; 
	double chi_B;  // chi variable in the transfer matrix formula
	double chi_L;
	double chi_P;
	double omega_B;  // omega variable in the transfer matrix formula
	double omega_L;
	double omega_P;
	double B_coeff;
	double L_coeff;
	double P_coeff;
	double f_plus;
	double f_minus;
	double B_coeff_plus;
	double B_coeff_minus;
	double L_coeff_plus;
	double L_coeff_minus;
	double P_coeff_plus;
	double P_coeff_minus;
	double mu_protein_plus;
	double mu_protein_minus;
	valarray<double> besseli_coeff_B;
	valarray<double> besseli_coeff_plus_B;
	valarray<double> besseli_coeff_minus_B;
	valarray<double> besseli_coeff_L;
	valarray<double> besseli_coeff_plus_L;
	valarray<double> besseli_coeff_minus_L;
	valarray<double> besseli_coeff_P;
	valarray<double> besseli_coeff_plus_P;
	valarray<double> besseli_coeff_minus_P;
	valarray<double> besseli_c_B;
	valarray<double> besseli_c_L;
	valarray<double> besseli_c_P;
	valarray<double> spherical_besseli_coeff_r_pr_f;
	valarray<double> spherical_besseli_coeff_r_pr_f_plus;
	valarray<double> spherical_besseli_coeff_r_pr_f_minus;
	valarray< complex<double> > coeff_1i_all;
	valarray< complex<double> > coeff_omega_B;
	valarray< complex<double> > coeff_omega_L;
	valarray< complex<double> > coeff_omega_P;
	valarray< complex<double> > coeff_1i_all_B;
	valarray< complex<double> > coeff_1i_all_L;
	valarray< complex<double> > coeff_1i_all_P;
	valarray<double> coeff_1i_all_T30;
	valarray<double> coeff_1i_all_T12;
	valarray<double> besseli_coeff_c_protein;
	// Possibly obsolete
	valarray< complex<double> > coeff_r_total_B;
	valarray< complex<double> > coeff_r_total_B_plus;
	valarray< complex<double> > coeff_r_total_B_minus;
	valarray< complex<double> > coeff_r_total_L;
	valarray< complex<double> > coeff_r_total_L_plus;
	valarray< complex<double> > coeff_r_total_L_minus;
	valarray< complex<double> > coeff_r_total_P;
	valarray< complex<double> > coeff_r_total_P_plus;
	valarray< complex<double> > coeff_r_total_P_minus;
};

bool isEmpty(string strInput);
string trimWhitespace(string strInput);
vector<double> separateStringDoubles(string strSequence);
void assignInput(string strLine, Params &p);
void recalculateForceTorque(Params &p, double force, double torque, double mu_protein, int max_mode);
Params initialiseParams(Input &input);
vector2d loadForceTorqueFile(string filename);

#endif
