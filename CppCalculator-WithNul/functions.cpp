#include "functions.h"

valarray<double> find_all_Besseli_r(double x, int max_n)
{
	valarray<double> varray(max_n + 1);

	for (int i = 0; i <= max_n; ++i)
		varray[i] = boost::math::cyl_bessel_i(i, x);

	return varray;
}

double modified_spherical_Bessel_i(int i, double x)
{
	if (x == 0)
	{
		if (i == 0)
			return 1.0;
		else
			return 0.0;
	}
	else
		return std::sqrt(pi / (2 * x)) * boost::math::cyl_bessel_i(i + 0.5, x);
}

valarray<double> find_all_spherical_Besseli_r(double x, int max_n)
{
	valarray<double> varray(max_n + 1);

	for (int i = 0; i <= max_n; ++i)
		varray[i] = modified_spherical_Bessel_i(i, x);

	return varray;
}

int index(int i1, int i2, int i3, int i4, int i5, int max_n)
{
	// For Wigner symbols
	int idx = i5 + (2 * max_n + 1) * i4 + std::pow(2 * max_n + 1, 2) * i3 + std::pow(2 * max_n + 1, 3) * i2 +
		std::pow(2 * max_n + 1, 3) * (max_n + 1) * i1;

	return idx;
}

vector<double> all_Wigner_3j_protein_torque(int max_n)
{
	int arr_size = std::pow(max_n + 1, 2) * std::pow(2 * max_n + 1, 3);
	vector<double> w3j_all(arr_size, 0.0);

	for (int i = 0; i <= max_n; ++i)
	{
		for (int j = 0; j <= max_n; ++j)
		{
			for (int k = std::abs(i - j); k <= i + j; ++k)
			{
				for (int r = -i; r <= i; ++r)
				{
					for (int t = -j; t <= j; ++t)
					{
						if (std::abs(r + t) <= k)
							w3j_all[index(i, j, k, max_n + r, max_n + t, max_n)] =
								WignerSymbols::wigner3j(i, j, k, r, t, -r - t);
					}
				}
			}
		}
	}

	return w3j_all;
}

long double factorial(int n)
{
	return (n == 1 || n == 0) ? 1 : n * factorial(n - 1);
}

double j_polynomial_my(double n, double alpha, double beta, double x)
{
	// LH: This function is modified, assumes only one evaluation point

	if (alpha <= -1.0)
		std::cerr << "We require alpha to be > -1\n";
	if (beta <= -1.0)
		std::cerr << "We require beta to be > -1\n";

	vector<double> v(n + 1, 1.0);

	if (n == 0)
		return 1.0;

	v[1] = (1.0 + 0.5 * (alpha + beta)) * x + 0.5 * (alpha - beta);

	for (int i = 2; i <= n; ++i)
	{
		double c1 = 2 * i * (i + alpha + beta) * (2 * i - 2 + alpha + beta);
		double c2 = (2 * i - 1 + alpha + beta) * (2 * i + alpha + beta) * (2 * i - 2 + alpha + beta);
		double c3 = (2 * i - 1 + alpha + beta) * (alpha + beta) * (alpha - beta);
		double c4 = -2 * (i - 1 + alpha) * (i - 1 + beta)  * (2 * i + alpha + beta);

		v[i] = ((c3 + c2 * x) * v[i - 1] + c4 * v[i - 2]) / c1;
	}

	double y = v.back();

	return y;
}

complex<double> P_n_ml_v2(double n, double m, double l, double theta)
{
	// program calculates P_n_ml polynomials

	// check conditions
	if (n < 0 || std::fmod(2 * n, 1) != 0 || std::fmod(2 * m, 1) != 0 || std::fmod(2 * l, 1) != 0 ||
		std::abs(m) > n || std::abs(l) > n || std::fmod(n + m, 1) != 0 || std::fmod(n + l, 1) != 0)
		std::cerr << "P_n_ml polynomials error\n";

	double a = std::abs(m - l);
	double b = std::abs(m + l);
	double k = n - (a + b) / 2;
	double lam = std::max(0.0, m - l);

	double cos_theta = std::cos(theta);
	double cos_theta_2 = std::cos(theta / 2);
	double sin_theta_2 = std::sin(theta / 2);

	double fact_result = factorial(k) * factorial(k + a + b) / 
		(factorial(k + a) * factorial(k + b));

	complex<double> p = std::pow(1i, m - l) * std::pow(-1, lam) * std::sqrt(fact_result) * 
		std::pow(sin_theta_2, a) * std::pow(cos_theta_2, b) * j_polynomial_my(k, a, b, cos_theta);

	return p;
}

double K_function(double x, double n, double l, double a)
{
	return std::exp(-a * (std::cos(x) - 1)) * P_n_ml_v2(n, l, l, x).real() * std::sin(x);
}

double L_k_r_v3(double n, double l, double a, double cutoff)
{
	if (n < 0 || std::abs(l) > n || std::fmod(n, 1) != 0 || std::fmod(l, 1) != 0)
		std::cerr << "K_n_l coefficients error\n";

	auto kfunc = [n, l, a](double x) { return K_function(x, n, l, a); };
	double coefficient = std::exp(-a) *
		boost::math::quadrature::gauss_kronrod<double, 61>::integrate(kfunc, 0.0, pi, 0, 0);
	// full sphere surface : cutoff = pi

	return coefficient;
}

vector2d find_indexed_L_k_r_v2(int max_n, int max_l, double a, double cutoff) 
{
	vector2d K(max_n + 1);

	for (int p = 0; p <= max_n; ++p)
	{
		K[p] = vector<double>(2 * max_l + 1, 0.0);

		for (int q = std::min(p, max_l); q >= -std::min(p, max_l); --q)
		{
			if (q >= 0)
				K[p][max_l + q] = L_k_r_v3(p, q, a, cutoff);
			else
				K[p][max_l + q] = K[p][max_l - q];
		}
	}

	return K;
}


int index(int i1, int i2, int i3, int max_n)
{
	// For D_n_ml and P_n_ml functions
	int idx = i3 + (2 * max_n - 1) * i2 + std::pow(2 * max_n - 1, 2) * i1;

	return idx;
}

vectorcd find_all_P_n_ml(int max_mode, double theta)
{
	vectorcd P(max_mode * std::pow(2 * max_mode - 1, 2));

	for (int p = 0; p < max_mode; ++p)
	{
		for (int q = -p; q <= p; ++q)
		{
			for (int r = -p; r <= p; ++r)
			{
				if (q <= 0 && std::abs(q) >= std::abs(r))
					P[index(p, max_mode + q - 1, max_mode + r - 1, max_mode)] = P_n_ml_v2(p, q, r, theta);
				else if (q <= 0 && std::abs(q) < std::abs(r))
				{
					if (r <= 0)
						P[index(p, max_mode + q - 1, max_mode + r - 1, max_mode)] = 
							P[index(p, max_mode + r - 1, max_mode + q - 1, max_mode)];
					else
						P[index(p, max_mode + q - 1, max_mode + r - 1, max_mode)] = 
							P[index(p, max_mode - r - 1, max_mode - q - 1, max_mode)];
				}
				else
					P[index(p, max_mode + q - 1, max_mode + r - 1, max_mode)] = 
						P[index(p, max_mode - q - 1, max_mode - r - 1, max_mode)];
			}
		}
	}

	return P;
}

vectorcd find_all_D_n_ml(int max_mode, double phi, double theta, double psi)
{
	// calculates all D_n_ml functions

	vectorcd D(max_mode * std::pow(2 * max_mode - 1, 2));
	vectorcd P = find_all_P_n_ml(max_mode, theta);

	for (int p = 0; p < max_mode; ++p)
	{
		for (int q = -p; q <= p; ++q)
		{
			for (int r = -p; r <= p; ++r)
				D[index(p, max_mode + q - 1, max_mode + r - 1, max_mode)] = std::exp(-1i * (q * phi + r * psi)) *
					P[index(p, max_mode + q - 1, max_mode + r - 1, max_mode)];
		}
	}

	return D;
}
